<?php $tahun_kegiatan = date("Y")-1;
switch($_GET[act]){
default:
?>
<div class="row example-print">
	<div class="col-sm-12">
    	<h1 style="text-align:center; text-transform:uppercase; margin-top:50px; margin-bottom:500px; font-size:30px;">
        <?php if($_SESSION[type_user]!='admin'){ ?>
        
        REKAPITULASI KEGIATAN <br /> BADAN PEMBINAAN HUKUM NASIONAL <br /> KEMENTERIAN HUKUM DAN HAM RI 
        <br /> TAHUN ANGGARAN <?php echo $tahun_kegiatan; ?><br /><br />
        
        <?php } else { ?>
        
        REKAPITULASI KEGIATAN <br /> BADAN PEMBINAAN HUKUM NASIONAL <br /> YANG BELUM DILAPORKAN <br /><br />
        
        <?php } ?>
        
        <img src="assets/img/logo_bphn.png"  style="height:250px;">
		</h1>
    </div>
</div>
<div class="row example-screen">
	<div class="col-sm-12">
        <?php if($_SESSION[type_user]=='admin'){ ?>
        <p>
            <a href="#" onclick="window.print();"><button class="btn btn-sm btn-default panel-title" style=""><span class="glyphicon glyphicon-print"></span> Cetak</button></a>
        </p>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">
                	Daftar Laporan <?php if($_SESSION[type_user]=='admin'){ ?> Yang Perlu Diapprove <?php }else {?> Yang Harus Dilaporkan dan Diupload<?php } ?>
                </h4>
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
    	<div class="table-responsive-vertical shadow-z-1" style="padding-top:5px;">
		<?php
		if($_SESSION[type_user]=='admin'){
			$daftar_pusat_tampil = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
        }else{
			$daftar_pusat_tampil = mysql_query("SELECT * FROM pusat WHERE id_pusat=$_SESSION[id_pusat]");
		}
		while($tp=mysql_fetch_array($daftar_pusat_tampil)){	?>
        <?php if($_SESSION[type_user]=='admin'){ ?>
        <h4 style="text-align:center; text-transform:uppercase; line-height: 150%;"> 
			<?php echo $tp['nama_pusat']; ?>
        </h4>
        <?php } ?>
        
		<?php 
		if($_SESSION[type_user]=='admin'){
			$daftar_laporan_kegiatan = mysql_query("SELECT count(tahun) as jumlah_laporan FROM datin_laporan 
													WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND pending=1 AND approve=0 ORDER BY id_laporan ASC");
		}else{
			$daftar_laporan_kegiatan = mysql_query("SELECT count(tahun) as jumlah_laporan FROM datin_laporan 
													WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND pending=0 AND approve=0 ORDER BY id_laporan ASC");
		}
		//$daftar_laporan_kegiatan=mysql_query("SELECT count(tahun) as jumlah_laporan FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND tahun=$tahun_kegiatan");
		$dkt = mysql_fetch_array($daftar_laporan_kegiatan); ?>
        <span style="font-size:12px; float:right; margin-right:15px;"><b><i>( <?php echo "$dkt[jumlah_laporan] Kegiatan"; ?> )</i></b></span><br />
        
        
          <!-- Table starts here -->
          <table id="table" class="table table-hover table-bordered <?php if($_SESSION[type_user]!='admin'){ ?>example-screen<?php } ?>">
              <thead style="text-transform:uppercase;">
                  <tr>
                    <th>No</th>
                    <th width="30%">Jenis Kegiatan</th>
                    <th width="5%">Tahun Kegiatan</th>
                    <th width="20%">Penanggung Jawab</th>
                    <th width="15%">Laporan Kegiatan <br /> <span style="font-size:10px; text-transform:capitalize;"></span></th>
                    <th width="20%">Komentar <?php if($_SESSION[type_user]!='admin'){ ?> Admin <?php }?></th>
                    <!--<th class="example-screen" width="1%">Aksi</th>-->
                  </tr>
              </thead>
              <tbody>
			<?php
            if($_SESSION[type_user]=='admin'){
                $tampil = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND pending=1 AND approve=0 ORDER BY id_laporan ASC");
            }else{
                $tampil = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND pending=0 AND approve=0 ORDER BY id_laporan ASC");
            }
            $cek_ada_ga=mysql_num_rows($tampil);
            if($cek_ada_ga!=0){
				$no=1;
				while($r=mysql_fetch_array($tampil)){?>
                        <?php
                        $ketua_sek=explode("/",$r['ketua_sek']);
                        $tgl_upload=tgl_indo($r[tgl_upload]);
                        $tgl_penyerahan=tgl_indo($r[tgl_hardcopy]);
                        ?>
                        <tr align="center">
                          <td data-title="No">															<?php echo $no; ?>						</td>
                          <td align="left" data-title="Kegiatan" style="text-transform:uppercase;">		<?php echo $r['judul_laporan']; ?>		</td>
                          <td data-title="Tahun" style="text-transform:uppercase;">		<?php echo $r['tahun']; ?>		</td>
                          <td data-title="Ketua/Sekretaris">
                          <form method='POST' action='?page=dashboard&act=<?php if($_SESSION[type_user]=='admin'){ echo "ket"; } else { echo"editnya"; }?>'  enctype='multipart/form-data'>
                    	  <input type='hidden' name='id' value='<?php echo $r['id_laporan']; ?>'>	
                                <?php if($_SESSION[type_user]=='admin'){ echo $r['ketua_sek']; } else {?>
                                <textarea name='ketua_sek' class="form-control example-screen" type='text' required="" style="text-align:center;"
                                placeholder='Ketua / Sekretaris'/><?php if(!empty($r['ketua_sek'])) { echo $r['ketua_sek']; }?></textarea>
                                <?php } ?>
                          </td>
                          <td data-title="File Fulltext" align="justify">
                                <?php if($_SESSION[type_user]=='admin'){ ?> 
										Nama File : <br /><a href="file_laporan/<?php echo $r['file_fulltext']; ?>" target="_blank" class="example-screen"><?php echo $r['file_fulltext']; ?> </a>
                                        				  <span class="example-print"><?php echo $r['file_fulltext']; ?></span>
                                              <br /><br />
                                        Tanggal Upload : <br /><?php echo $tgl_upload; ?> 
								<?php } else {?>
                                        <input type='file' name='fupload' required="">
                                        <span style="font-size:10px;">
                                        <!--*) file dapat berupa .rar / .zip/ file compress yang  berisi cover sampai penutup. 
                                        akan lebih baik jika dalam 1 file PDF atau word yang sudah terdiri dari Cover sampai penutup (jika diperlukan menggunakan watermark juga dipersilahkan)-->
                                        (Format PDF dalam 1 File tersusun sesuai sistematika dari cover sampai lampiran)
                                        </span>
                                <?php } ?>
                                <br /><br />
                                <div class="example-screen">
                                    <button class="btn btn-default" type="submit" style="width:100%;">
                                        <?php if($_SESSION[type_user]=='admin'){ ?>
                                                    <span class="glyphicon glyphicon-envelope"></span> Kirimkan Pesan
                                        <?php } else { ?>
                                                    <span class="glyphicon glyphicon-save"></span> Proses
                                        <?php }?>
                                    </button>
                                    <?php if($_SESSION[type_user]=='admin'){ ?>
                                            <a href="?page=dashboard&act=approve&id=<?php echo $r['id_laporan']; ?>">
                                            <span class="btn btn-default" style="width:100%; margin-top:10px;">
                                                <span class="glyphicon glyphicon-ok"></span> Approve
                                            </span>
                                            </a>
                                    <?php } ?>
                                </div>
                          </td>
                          <td data-title="Keterangan" align="justify">
                                <?php if($_SESSION[type_user]=='admin'){ ?> 
										<textarea name='ket' class="form-control example-screen" type='text' rows="4" 
                                        	placeholder='Isikan Jika Fulltext tidak sesuai harapan'/><?php if(!empty($r['ket'])) { echo $r['ket']; }?></textarea> 
								<?php } else {?>
                                		<?php if(!empty($r['ket'])) { echo $r['ket']; }?>
                                <?php } ?>
                          </td>
                          <!--<td class="example-screen">
                                <button class="btn btn-default" type="submit" style="width:100%;">
                                    <?php if($_SESSION[type_user]=='admin'){ ?>
                                    			<span class="glyphicon glyphicon-envelope"></span> Kirimkan Pesan
									<?php } else { ?>
												<span class="glyphicon glyphicon-save"></span> Proses
									<?php }?>
                                </button>
                                <?php if($_SESSION[type_user]=='admin'){ ?>
                                        <a href="?page=dashboard&act=approve&id=<?php echo $r['id_laporan']; ?>">
                                        <span class="btn btn-default" style="width:100%; margin-top:10px;">
                                            <span class="glyphicon glyphicon-ok"></span> Approve
                                        </span>
                                        </a>
								<?php } ?>
                                
                          
                          </td>-->
                        </tr>
                        </form>
                <?php $no++;
				} // tutup while daftar laporan?>
        <?php } // tutup if cek ga ada data
			  else 	{?>
              		<tr align="center">
                          <td colspan="6">Tidak Ada</td>
                    </tr>
		<?php }?>
              </tbody>
            </table>
            <?php if($_SESSION[type_user]=='admin'){ ?><div class="example-print" style="margin-bottom:1000px;"></div><?php } ?>
     <?php } //end while pusat eselon 1?> 
    	</div>
    </div>
</div>
<!-- ======================================================================================================================================================================================= -->
<?php if($_SESSION[type_user]!='admin'){ ?>
<div class="row example-screen">
	<div class="col-sm-12">
        <p>
            <a href="#" onclick="window.print();"><button class="btn btn-sm btn-default panel-title" style=""><span class="glyphicon glyphicon-print"></span> Cetak</button></a>
        </p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
    	<div class="table-responsive-vertical shadow-z-1" style="padding-top:5px;">
		<?php
		$daftar_pusat_tampil = mysql_query("SELECT * FROM pusat WHERE id_pusat=$_SESSION[id_pusat]");
        $tp=mysql_fetch_array($daftar_pusat_tampil);
        ?>
        
        <h4 style="text-align:center; text-transform:uppercase; line-height: 150%;">
            <?php echo $tp['nama_pusat']; ?>
            <br /> TAHUN ANGGARAN <?php echo $tahun_kegiatan; ?>
			
			<?php 
			$dkt = mysql_fetch_array(mysql_query("SELECT count(tahun) as jumlah_laporan FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND tahun=$tahun_kegiatan"));
			?>
            <!--<br /> <span style="font-size:12px; float:right; margin-right:15px;"><b><i>( <?php echo "$dkt[jumlah_laporan] Kegiatan"; ?>)</i></b></span>-->
        </h4>
        
          <!-- Table starts here -->
          <table id="table" class="table table-hover table-bordered">
              <thead style="text-transform:uppercase;">
                  <tr>
                    <th rowspan="2" width="1%">No</th>
                    <th rowspan="2" width="1%"></th>
                    <th rowspan="2">Jenis Kegiatan</th>
                    <th rowspan="2">Akronim</th>
                    <th rowspan="2" width="15%">Penanggung Jawab</th>
                    <th colspan="2" width="40%">Keterangan</th>
                  </tr>
                  <tr>
                    <th>Softcopy</th>
                    <th  width="15%">Data Penyerahan Hardcopy</th>
                  </tr>
              </thead>
              <tbody>
              	<?php
				$tampil = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND tahun=$tahun_kegiatan AND id_laporan_parent=0 ORDER BY id_laporan ASC");
				
				$no=1;
				while($r=mysql_fetch_array($tampil)){?>
					<?php
                    if($r[type]=='bab'){?>
						<tr align="center">
                          <td data-title="No"><b>									<?php echo $no; ?>						</b></td>
                          <td align="left" data-title="Kegiatan" colspan="6"><b>	<?php echo $r['judul_laporan']; ?> :	</b></td>
                        </tr>
                        <?php
						$cek_laporan_parent = mysql_query("SELECT * FROM datin_laporan WHERE id_laporan_parent=$r[id_laporan] ORDER BY id_laporan ASC");
						$cekjml=mysql_num_rows($cek_laporan_parent);
						if($cekjml!=0){
							$no1=1;
							while($lparent2=mysql_fetch_array($cek_laporan_parent)){?>
								<?php
                                if($lparent2[type]=='bab'){?>
                                    <tr align="center">
                                      <td></td>
                                      <td align="left" data-title="Kegiatan" colspan="6"><b>	<?php echo $lparent2['judul_laporan']; ?> :		</b></td>
                                    </tr>
                                    <?php
                                    $cek_laporan_parent2 = mysql_query("SELECT * FROM datin_laporan WHERE id_laporan_parent=$lparent2[id_laporan] ORDER BY id_laporan ASC");
                                    $cekjml2=mysql_num_rows($cek_laporan_parent2);
                                    if($cekjml2!=0){
                                        $no2=1;
                                        while($lparent3=mysql_fetch_array($cek_laporan_parent2)){?>
											<?php
                                            if($lparent3[type]=='bab'){?>
                                                <tr align="center">
                                                  <td></td>
                                                  <td align="left" data-title="Kegiatan" colspan="6"><b>	<?php echo $lparent3['judul_laporan']; ?> :		</b></td>
                                                </tr>
                                                <?php
                                                $cek_laporan_parent3 = mysql_query("SELECT * FROM datin_laporan WHERE id_laporan_parent=$lparent3[id_laporan] ORDER BY id_laporan ASC");
                                                $cekjml3=mysql_num_rows($cek_laporan_parent3);
                                                if($cekjml3!=0){
                                                    $no3=1;
                                                    while($lparent4=mysql_fetch_array($cek_laporan_parent3)){
                                                        
                                                    }
                                                }//tutup if paren3
                                                ?>
                                            <?php
                                            }// tutup if daftar bab
                                            else{
                                            ?>
                                                <?php
                                                $ketua_sek3=explode("/",$lparent3['ketua_sek']);
                                                $tgl_upload3=tgl_indo($lparent3[tgl_upload]);
                                                $tgl_penyerahan3=tgl_indo($lparent3[tgl_hardcopy]);
                                                ?>
                                                <tr align="center">
                                                  <td></td>
                                                  <td data-title="No">							<?php echo $no2; ?>).						</td>
                                                  <td align="left" data-title="Kegiatan">		<?php echo $lparent3['judul_laporan']; ?>	</td>
                                                  <td data-title="Akronim">						
												  		<?php if(!empty($lparent3['akronim'])){ echo $lparent3['akronim']; } else {?> <!--Belum ada Data--> <?php } ?>			
                                                  </td>
                                                  <td data-title="Ketua/Sekretaris">			
                                                        <?php 	if(!empty($lparent3['ketua_sek'])){
                                                                    if(empty($ketua_sek3[1])) { echo "Ketua :<br>$ketua_sek3[0]"; }
																	elseif(empty($ketua_sek3[0])) { echo "Sekretaris :<br>$ketua_sek3[1]"; } 
                                                                    else { echo "Ketua :<br> ".$ketua_sek3[0]." <br><br> Sekretaris :<br> ".$ketua_sek3[1].""; } 
                                                                }else { /*echo "Belum ada Data";*/ }
                                                        ?>
                                                  </td>
                                                  <td data-title="Softcopy">
                                                        <?php if(!empty($lparent3['file_fulltext'])){ ?>
                                                            Nama File : <br /><a href="file_laporan/<?php echo $lparent3['file_fulltext']; ?>" target="_blank">
																				<?php echo $lparent3['file_fulltext']; ?> </a>
                                                                  <br /><br />
                                                            Tanggal Upload : <br /><?php echo $tgl_upload3 ?>
                                                        <?php } else {/*echo "Belum ada Data yang diupload";*/ }?>
                                                  </td>
                                                  <td data-title="Hardcopy">
                                                        <?php if(!empty($lparent3['eksemplar'])){ ?>
                                                            Jumlah : <br /><?php echo $lparent3['eksemplar']; ?> Eksemplar<br /><br />
                                                            Tanggal Penyerahan : <br /><?php echo $tgl_penyerahan3 ?>
                                                        <?php } else {/*echo "Belum ada Data Laporan Kegiatan yang diserahkan";*/ }?>
                                                  </td>
                                                </tr>
                                            <?php 
                                            }// tutup else daftar laporan
                                            ?>
                                        <?php $no2++;
                                        } // tutup while daftar laporan
                                    }//tutup if paren2
                                    ?>
                                <?php
                                }// tutup if daftar bab
                                else{
                                ?>
                                    <?php
                                    $ketua_sek2=explode("/",$lparent2['ketua_sek']);
                                    $tgl_upload2=tgl_indo($lparent2[tgl_upload]);
                                    $tgl_penyerahan2=tgl_indo($lparent2[tgl_hardcopy]);
                                    ?>
                                    <tr align="center">
                                      <td></td>
                                      <td data-title="No">							<?php echo $no1; ?>).						</td>
                                      <td align="left" data-title="Kegiatan"> 		<?php echo $lparent2['judul_laporan']; ?>	</td>
                                      <td data-title="Akronim">						<?php echo $lparent2['akronim']; ?>			</td>
                                      <td data-title="Ketua/Sekretaris">			
                                            <?php 	if(!empty($lparent2['ketua_sek'])){
                                                        if(empty($ketua_sek2[1])) { echo "Ketua :<br>$ketua_sek2[0]"; } elseif(empty($ketua_sek2[0])) { echo "Sekretaris :<br>$ketua_sek2[1]"; } 
                                                        else { echo "Ketua :<br> ".$ketua_sek2[0]." <br><br> Sekretaris :<br> ".$ketua_sek2[1].""; } 
                                                    } else {/*echo "Belum ada Data";*/ }
                                            ?>
                                      </td>
                                      <td data-title="Softcopy">
                                            <?php if(!empty($lparent2['file_fulltext'])){ ?>
                                                Nama File : <br /><a href="file_laporan/<?php echo $lparent2['file_fulltext']; ?>" target="_blank">
																				<?php echo $lparent2['file_fulltext']; ?> </a>
                                                                  <br /><br />
                                                Tanggal Upload : <br /><?php echo $tgl_upload2 ?>
                                            <?php } else {/*echo "Belum ada Data yang diupload"; */}?>
                                      </td>
                                      <td data-title="Hardcopy">
                                            <?php if(!empty($lparent2['eksemplar'])){ ?>
                                                Jumlah : <br /><?php echo $lparent2['eksemplar']; ?> Eksemplar<br /><br />
                                                Tanggal Penyerahan : <br /><?php echo $tgl_penyerahan2 ?>
                                            <?php } else {/*echo "Belum ada Data Laporan Kegiatan yang diserahkan";*/ }?>
                                      </td>
                                    </tr>
                                <?php 
                                }// tutup else daftar laporan
                                ?>
                            <?php $no1++;
                            } // tutup while daftar laporan
						}//tutup if parent1
						?>
                    <?php
                    }// tutup if daftar bab
                    else{
                    ?>
                        <?php
                        $ketua_sek=explode("/",$r['ketua_sek']);
                        $tgl_upload=tgl_indo($r[tgl_upload]);
                        $tgl_penyerahan=tgl_indo($r[tgl_hardcopy]);
                        ?>
                        <tr align="center">
                          <td data-title="No">	<b>						<?php echo $no; ?>						</b></td>
                          <td align="left" data-title="Kegiatan" style="text-transform:uppercase;" colspan="2"><b>	<?php echo $r['judul_laporan']; ?>		</b></td>
                          <td data-title="Akronim">						<?php echo $r['akronim']; ?>			</td>
                          <td data-title="Ketua/Sekretaris">			
                                <?php 	if(!empty($r['ketua_sek'])){
                                            if(empty($ketua_sek[1])) { echo "Ketua :<br>$ketua_sek[0]"; } elseif(empty($ketua_sek[0])) { echo "Sekretaris :<br>$ketua_sek[1]"; } 
                                            else { echo "Ketua :<br> ".$ketua_sek[0]." <br><br> Sekretaris :<br> ".$ketua_sek[1].""; } 
                                        }else {/*echo "Belum ada Data";*/ }
                                ?>
                          </td>
                          <td data-title="Softcopy">
                                <?php if(!empty($r['file_fulltext'])){ ?>
                                    Nama File : <br /><a href="file_laporan/<?php echo $r['file_fulltext']; ?>" target="_blank">
																				<?php echo $r['file_fulltext']; ?> </a>
                                                                  <br /><br />
                                    Tanggal Upload : <br /><?php echo $tgl_upload ?>
                                <?php } else {/*echo "Belum ada Data yang diupload";*/ }?>
                          </td>
                          <td data-title="Hardcopy">
                                <?php if(!empty($r['eksemplar'])){ ?>
                                    Jumlah : <br /><?php echo $r['eksemplar']; ?> Eksemplar<br /><br />
                                    Tanggal Penyerahan : <br /><?php echo $tgl_penyerahan ?>
                                <?php } else {/*echo "Belum ada Data Laporan Kegiatan yang diserahkan";*/ }?>
                          </td>
                        </tr>
                    <?php 
                    }// tutup else daftar laporan
                    ?>
                <?php $no++;
				} // tutup while daftar laporan?>
              </tbody>
            </table>
           
    	</div>
    </div>
</div>
<?php } ?>
<?php
break;
//==========================================================================================================================
case "editnya";

	$lokasi_file    = $_FILES['fupload']['tmp_name'];
	$tipe_file      = $_FILES['fupload']['type'];
	$nama_file      = $_FILES['fupload']['name'];

	move_uploaded_file($lokasi_file,"file_laporan/$nama_file");
	mysql_query("UPDATE datin_laporan SET ketua_sek  = '$_POST[ketua_sek]', file_fulltext = '$nama_file', tgl_upload  = '$tgl_sekarang', pending = 1
				  WHERE id_laporan     = '$_POST[id]'");

echo "<script language='JavaScript'>location.href='?page=dashboard';</script>";

//echo"'$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";

break;
//================================================================================================================================
case "ket";

	mysql_query("UPDATE datin_laporan SET tgl_upload  = 0, file_fulltext = '', pending = 0, ket = '$_POST[ket]' WHERE id_laporan     = '$_POST[id]'");

echo "<script language='JavaScript'>location.href='?page=dashboard';</script>";

//echo"'$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";

break;
//================================================================================================================================
case "approve";

mysql_query("UPDATE datin_laporan SET tgl_approve  = '$tgl_sekarang', approve = 1 WHERE id_laporan     = '$_GET[id]'");

echo "<script language='JavaScript'>location.href='?page=dashboard';</script>";

//echo"'$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";

break;
//================================================================================================================================
}
?>
