<script type="text/javascript">
$(document).ready(function()
{
	$(".country").change(function()
	{
		var id=$(this).val();
		var dataString = 'id='+ id;
	
		$.ajax
		({
			type: "POST",
			url: "get_state.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".state").html(html);
			} 
		});
	});
	
	
	$(".state").change(function()
	{
		var id=$(this).val();
		var dataString = 'id='+ id;
	
		$.ajax
		({
			type: "POST",
			url: "get_city.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".city").html(html);
			} 
		});
	});
	
});
</script>
<?php
switch($_GET[act]){
  // Tampil contact
  default:
?>
<div class="row example-print">
	<div class="col-sm-12">
    	<h1 style="text-align:center; text-transform:uppercase; margin-top:50px; margin-bottom:500px; font-size:34px;">
        REKAPITULASI KEGIATAN <br /> BADAN PEMBINAAN HUKUM NASIONAL <br /> KEMENTERIAN HUKUM DAN HAM RI <br /> 
        TAHUN ANGGARAN <?php if(!empty($_GET[tahun])){echo $_GET['tahun'];}else{ echo date("Y");} ?> <br /><br />
        <img src="assets/img/logo_bphn.png"  style="height:250px;">
		</h1>
    </div>
</div>
<div id="openModalkepala" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Penambahan Data Laporan Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=rekapitulasi&act=tambah&tipe=bab'>
                            <div class="row">
                                <div class="col-sm-4">
                                    Pilih Eselon II
                                    <select class="form-control" name="id_pusat">
										<?php $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
                                        while ($p=mysql_fetch_array($daftar_pusat)){ ?>
                                        <option value="<?php echo $p[id_pusat]; ?>"> <?php echo $p['nama_pusat']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div> 
                                <div class="col-sm-4">
                                	Judul Kepala Kegiatan
                                    <input name='judul_laporan' class="form-control" type='text' placeholder='Judul Kepala Kegiatan' required="" />
                                </div>
                                <div class="col-sm-2">
                                	Pilih Tahun Kegiatan
                                    <input name='tahun' class="form-control" type='text' placeholder='Tahun' required="" onKeyUp="validAngka(this)" />
                                </div>
                                <div class="col-sm-2" style="text-align:center;"> &nbsp;
                                	<button class="btn btn-default" type="submit" style="width:100%;">
                                        <span class="glyphicon glyphicon-save"></span> Simpan
                                    </button>
                                </div>                            
                        	</div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<div id="openModallaporan" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Penambahan Data Laporan Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=rekapitulasi&act=tambah'>
                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-sm-4">
                                    Pilih Eselon II
                                    <select class="form-control" name="id_pusat">
                                        <?php
                                        $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
                                        while ($p=mysql_fetch_array($daftar_pusat)){ 
                                        ?>
                                        <option value="<?php echo $p[id_pusat];?>"> <?php echo $p['nama_pusat']; ?> </option>
                                        <?php } ?>
                                    </select> 
                                </div>
                                <div class="col-sm-1">
                                    Tahun
                                    <input name='tahun' class="form-control" type='text' placeholder='Tahun' required="" onKeyUp="validAngka(this)" />
                                </div>
                                <div class="col-sm-7">
                                    Judul Kegiatan
                                    <input name='judul_laporan' class="form-control" type='text' placeholder='Judul Laporan' required=""/>
                                </div>
                        	</div>
                            <div class="row">
                                <div class="col-sm-4">
                                    Masukan Akronim
                                    <input name='akronim' class="form-control" type='text' placeholder='Akronim' required="" />
                                </div>
                                <div class="col-sm-6">
                                    Masukan Ketua / Sekretaris
                                    <input name='ketua_sek' class="form-control" type='text' placeholder='Ketua / Sekretaris' required="" />
                                </div>
                                <div class="col-sm-2" style="text-align:center;"> &nbsp;
                                        <button class="btn btn-default" type="submit" style="width:100%;">
                                            <span class="glyphicon glyphicon-save"></span> Simpan
                                        </button>
                                </div>                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<!-- Bagian Open Modal ================================================================================================================================ -->

<div class="row example-screen">
	<div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pencarian Berdasarkan Nama/Judul Kegiatan</h4>
            </div>
            <div class="panel-body">
                <form method='POST' action='?page=remun&act=tambah'>
                    <div class="input-group">
                        <label class="sr-only" for="search-form">Search the site</label>
                        <input type="text" class="form-control" id="search-form" required="">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                                <span class="sr-only">Search</span>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pencarian Berdasarkan Eselon II</h4>
            </div>
            <div class="panel-body">
                <div class="input-group">
                    <label class="sr-only" for="search-form">Search the site</label>
                    <select class="form-control" onchange="window.open(this.options[this.selectedIndex].value,'_top')">
						<option value=""> Pilih Eselon II </option>
						<?php $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
                        while ($p=mysql_fetch_array($daftar_pusat)){ ?>
                        <option value="<?php echo $p[id_pusat]; ?>"> 
                            <?php echo $p['nama_pusat']; ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-sm-2">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pencarian Tahun</h4>
            </div>
            <div class="panel-body">
                <div class="input-group">
                    <label class="sr-only" for="search-form">Search the site</label>
                    <select class="form-control" onchange="window.open(this.options[this.selectedIndex].value,'_top')">
						<option value=""> Pilih Tahun </option>
						<?php $daftar_tahun = mysql_query("SELECT tahun,count(tahun) as jumlah_laporan from datin_laporan group by tahun");
                        while ($dt=mysql_fetch_array($daftar_tahun)){ ?>
                        <option value="<?php echo $dt[tahun]; ?>"> 
                            <?php echo "$dt[tahun] ( $dt[jumlah_laporan] Kegiatan )"; ?>
                        </option>";
                        <?php } ?>
                    </select>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row example-screen">
	<div class="col-sm-12">
        <p>
            <a href="#" onclick="window.print();"><button class="btn btn-sm btn-default panel-title" style=""><span class="glyphicon glyphicon-print"></span> Cetak</button></a>
            <a href="#openModalkepala"><button class="btn btn-sm btn-default panel-title"><span class="glyphicon glyphicon-plus"></span> Data Judul Kepala Kegiatan</button></a>
            <a href="#openModallaporan"><button class="btn btn-sm btn-default panel-title"><span class="glyphicon glyphicon-plus"></span> Data Laporan Kegiatan</button></a>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Daftar Laporan</h4>
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
    	<div class="table-responsive-vertical shadow-z-1">
  <!-- Table starts here -->
  <table id="table" class="table table-hover table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Link</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td data-title="ID">1</td>
          <td data-title="JENIS KEGIATAN : ">Pengkajian Hukum tentang Tinjauan Terhadap Efisiensi Pelaksanan Pemilu di Indonesia</td>
          <td data-title="Link">
            <a href="https://github.com/zavoloklom/material-design-color-palette" target="_blank">GitHub</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">2</td>
          <td data-title="Name">Material Design Iconic Font</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/uqCsB" target="_blank">Codepen</a>
            <a href="https://github.com/zavoloklom/material-design-iconic-font" target="_blank">GitHub</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">3</td>
          <td data-title="Name">Material Design Hierarchical Display</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/eNaEBM" target="_blank">Codepen</a>
            <a href="https://github.com/zavoloklom/material-design-hierarchical-display" target="_blank">GitHub</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">4</td>
          <td data-title="Name">Material Design Sidebar</td>
          <td data-title="Link"><a href="http://codepen.io/zavoloklom/pen/dIgco" target="_blank">Codepen</a></td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">5</td>
          <td data-title="Name">Material Design Tiles</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/wtApI" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">6</td>
          <td data-title="Name">Material Design Typography</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/IkaFL" target="_blank">Codepen</a>
            <a href="https://github.com/zavoloklom/material-design-typography" target="_blank">GitHub</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">7</td>
          <td data-title="Name">Material Design Buttons</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/Gubja" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">In progress</td>
        </tr>
        <tr>
          <td data-title="ID">8</td>
          <td data-title="Name">Material Design Form Elements</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/yaozl" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">In progress</td>
        </tr>
        <tr>
          <td data-title="ID">9</td>
          <td data-title="Name">Material Design Email Template</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/qEVqzx" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
        <tr>
          <td data-title="ID">10</td>
          <td data-title="Name">Material Design Animation Timing (old one)</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/Jbrho" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
         <tr>
          <td data-title="ID">10</td>
          <td data-title="Name">Material Design Animation Timing (old one)</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/Jbrho" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
         <tr>
          <td data-title="ID">10</td>
          <td data-title="Name">Material Design Animation Timing (old one)</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/Jbrho" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
         <tr>
          <td data-title="ID">10</td>
          <td data-title="Name">Material Design Animation Timing (old one)</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/Jbrho" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
         <tr>
          <td data-title="ID">10</td>
          <td data-title="Name">Material Design Animation Timing (old one)</td>
          <td data-title="Link">
            <a href="http://codepen.io/zavoloklom/pen/Jbrho" target="_blank">Codepen</a>
          </td>
          <td data-title="Status">Completed</td>
        </tr>
      </tbody>
    </table>
  </div>
    </div>
</div>
<?php
break;
//==========================================================================================================================
case "tambah";?>
<div class="row example-screen" align="center">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Penambahan Data Laporan Kegiatan</h4>
            </div>
            <div class="panel-body">
                <form method='POST' action='?page=rekapitulasi&act=tambah&tipe=laporan'>
                    <div class="row">
                        <div class="col-sm-4">
                            Pilih Eselon II
                            <select class="form-control" name="id_pusat" onchange="window.open(this.options[this.selectedIndex].value,'_top')">
                                <?php 
								if(empty($_GET[id_pusat])){ $query=$_POST[id_pusat]; } else { $query=$_GET[id_pusat]; }
								$cek_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat = ".$query." ORDER BY id_pusat ASC");
								$fisrt=mysql_fetch_array($cek_pusat); ?>
								<option value="?page=rekapitulasi&act=tambah&id_pusat=<?php echo $fisrt[id_pusat];?>"> <?php echo $fisrt['nama_pusat']; ?> </option>
								<option value="?page=rekapitulasi&act=tambah&id_pusat=<?php echo $fisrt[id_pusat];?>"> ----------------------------------- </option>
								<?php
                                $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
								while ($p=mysql_fetch_array($daftar_pusat)){ 
								?>
                                <option value="?page=rekapitulasi&act=tambah&id_pusat=<?php echo $p[id_pusat];?>"> <?php echo $p['nama_pusat']; ?> </option>
                                <?php } ?>
                            </select> 
                        </div>
                        <div class="col-sm-2">
                            Pilih Kepala Kegiatan
                            <span name='divWilayah' id='divWilayah'>
                                <select name='id_laporan_parent' class="form-control">
                                    <option value='' selected>Tidak ada</option>
                                    <?php 
									$daftar_bab = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat = ".$query." ORDER BY id_pusat ASC");
                                    while ($bab=mysql_fetch_array($daftar_bab)){ ?>
                                    <option value="<?php echo $bab[id_laporan]; ?>"> <?php echo $bab['judul_laporan']; ?> </option>
                                    <?php } ?> 
                                </select>
                                </span>
                        </div>
                        <div class="col-sm-1">
                            Tahun
                            <input name='tahun' class="form-control" type='text' placeholder='Tahun' required="" onKeyUp="validAngka(this)" />
                        </div>
                        <div class="col-sm-2">
                            Masukan Akronim
                            <input name='akronim' class="form-control" type='text' placeholder='Akronim' required="" onKeyUp="validAngka(this)" />
                        </div>
                        <div class="col-sm-2">
                            Masukan Ketua / Sekretaris
                            <input name='ketua_sek' class="form-control" type='text' placeholder='Ketua / Sekretaris' required="" />
                        </div>
                        <div class="col-sm-1" style="text-align:center;"> &nbsp;
                                <button class="btn btn-default" type="submit" style="width:100%;">
                                    <span class="glyphicon glyphicon-save"></span> Simpan
                                </button>
                        </div>                            
                	</div>
                </form>
            </div>
        </div>
     </div>
</div>
<?php
break;
//=============================================================================================================================
case"tambahnya"; 

mysql_query("INSERT INTO datin_laporan(id_pusat, judul_laporan, tahun, tipe) VALUES('$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', 'bab')");
echo "<script language='JavaScript'>location.href='?page=rekapitulasi';</script>";

break;
//=============================================================================================================================
}
?>
