<script type="text/javascript">
$(document).ready(function(){
	$(".pusat").change(function()
	{
		var id=$(this).val();
		var dataString = 'id='+ id;
	
		$.ajax
		({
			type: "POST",
			url: "get_bab.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".laporan_parent").html(html);
			} 
		});
	});	
});
</script>
<?php
if(!empty($_GET[tahun])){ $tahun_kegiatan = $_GET[tahun]; } else{ $tahun_kegiatan = date("Y")-1; }
switch($_GET[act]){
  // Tampil contact
default:
$ketemu_judul=1;
if($_POST['action']=='cari'){
	$jlcek=str_replace("&sbquo;",",",$_POST['judul_laporan']);
	$cari_judul	= mysql_query("SELECT * FROM datin_laporan  WHERE judul_laporan LIKE '%$jlcek%'");
	$cek_judul=mysql_num_rows($cari_judul);
	if($cek_judul == 0){ 
		echo "<script language='JavaScript'>location.href='?page=rekapitulasi#openModalgagal';</script>"; 
		$ketemu_judul=0; 
	}else{$query_judul=1;}
}
?> 
<div class="row example-print">
	<div class="col-sm-12">
    	<h1 style="text-align:center; text-transform:uppercase; margin-top:50px; margin-bottom:500px; font-size:34px;">
        REKAPITULASI KEGIATAN <br /> BADAN PEMBINAAN HUKUM NASIONAL <br /> KEMENTERIAN HUKUM DAN HAM RI 
        <br /> TAHUN ANGGARAN <?php echo $tahun_kegiatan; ?><br /><br />
        <img src="assets/img/logo_bphn.png"  style="height:250px;">
		</h1>
    </div>
</div>
<div id="openModalgagal" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;color:#FF0000;">Judul Laporan Kegiatan Tidak Ditemukan</h4>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<div id="openModallaporan" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Penambahan Data Laporan Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=rekapitulasi&act=tambah'>
                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-sm-2">
                                    Tipe Laporan
                                    <select class="form-control" name="type">
                                        <option  selected="selected">Laporan</option>
                                        <option value="bab">Kepala Kegiatan</option>
                                    </select> 
                                </div>
                                <div class="col-sm-4">
                                    Pilih Eselon II
                                    <select class="form-control pusat" name="id_pusat" >
                                        <option selected="selected">-- Pilih Eselon II --</option>
										<?php
                                        $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
                                        while ($p=mysql_fetch_array($daftar_pusat)){ 
                                        ?>
                                        <option value="<?php echo $p[id_pusat];?>"> <?php echo $p['nama_pusat']; ?> </option>
                                        <?php } ?>
                                    </select> 
                                </div>
                                <div class="col-sm-6">
                                    Pilih Jenis Kepala Kegiatan
                                    <select class="form-control laporan_parent" name="id_laporan_parent">
                                        <option selected="selected">-- Pilih Kepala Kegiatan --</option>
                                    </select>
                                </div>
                       		</div>
                            <div class="row">
                                <div class="col-sm-1">
                                    Tahun
                                    <input name='tahun' class="form-control" type='text' placeholder='Tahun' required="" onKeyUp="validAngka(this)" />
                                </div>
                                <div class="col-sm-5">
                                    Judul Kegiatan
                                    <input name='judul_laporan' class="form-control" type='text' placeholder='Judul Laporan' required=""/>
                                </div>
                                <div class="col-sm-2">
                                    Akronim
                                    <input name='akronim' class="form-control" type='text' placeholder='Akronim Laporan'/>
                                </div>
                                <div class="col-sm-3">
                                    Ketua / Sekretaris
                                    <input name='ketua_sek' class="form-control" type='text' placeholder='Ketua / Sekretaris'/>
                                </div>
                                <div class="col-sm-1" style="text-align:center;"> &nbsp;
                                        <button class="btn btn-default" type="submit" style="width:100%;">
                                            <span class="glyphicon glyphicon-save"></span> Simpan
                                        </button>
                                </div>
                        	</div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<div id="openModalperingatan" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Berikan Informasi Peringatan</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=rekapitulasi&act=tambahperingatan'>
                            <div class="row" style="margin-bottom:20px;">
                                <div class="col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> Upload File Surat</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type='file' name='fupload' required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-sm-4">
                                	<div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> Aktifkan peringatan ini</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="checkbox" name="aktif" value="Aktiv" required="required"> Aktif
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                </div>
                                <div class="col-sm-4"><br />
                                    <button class="btn btn-default" type="submit" style="width:100%;">
                                        <span class="glyphicon glyphicon-save"></span> Simpan
                                    </button>
                                </div>
                       		</div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<!-- Bagian Open Modal -->

<div class="row example-screen">
	<div class="col-sm-5">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pencarian Berdasarkan Nama/Judul Kegiatan</h4>
            </div>
            <div class="panel-body">
                <form method='POST' action='?page=rekapitulasi'>
                	<input type='hidden' name='action' value='cari'>
                    <div class="input-group">
                        <label class="sr-only" for="search-form">Search the site</label>
                        <input type="text" class="form-control awesomplete" id="search-form" name="judul_laporan" required="" tabindex="1"
                          data-list="<?php
                        $listjudullaporan	= mysql_query("SELECT * FROM datin_laporan WHERE type != 'bab' ORDER BY id_laporan ASC");
						while($listjl=mysql_fetch_array($listjudullaporan)){
                        $jl=str_replace(",","&sbquo;",$listjl['judul_laporan']);
                        echo $jl.",";
                        }?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                                <span class="sr-only">Search</span>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pencarian Berdasarkan Eselon II</h4>
            </div>
            <div class="panel-body">
                <div class="input-group">
                    <label class="sr-only" for="search-form">Search the site</label>
                    <select class="form-control" onchange="window.open(this.options[this.selectedIndex].value,'_top')" style="margin-top:3px; margin-bottom:5px;" tabindex="2">
						<option value=""> Pilih Eselon II </option>
						<?php $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
                        while ($p=mysql_fetch_array($daftar_pusat)){ ?>
                        <option value="?page=rekapitulasi&filter=pusat&id_pusat=<?php echo $p[id_pusat]; ?>"> 
                            <?php echo $p['nama_pusat']; ?>
                        </option>
                        <?php } ?>
                        <option value="?page=rekapitulasi"> Tampilkan Semua </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-sm-3">
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pencarian Tahun</h4>
            </div>
            <div class="panel-body">
                <div class="input-group">
                    <label class="sr-only" for="search-form">Search the site</label>
                    <select class="form-control" onchange="window.open(this.options[this.selectedIndex].value,'_top')" style="margin-top:3px; margin-bottom:5px;" tabindex="3">
						<option value=""> Pilih Tahun </option>
						<?php $daftar_tahun = mysql_query("SELECT tahun,count(tahun) as jumlah_laporan FROM datin_laporan WHERE type != 'bab' GROUP BY tahun");
                        while ($dt=mysql_fetch_array($daftar_tahun)){ ?>
                        <option value="?page=rekapitulasi&filter=tahun&tahun=<?php echo $dt[tahun]; ?>"> 
                            <?php echo "$dt[tahun] ( $dt[jumlah_laporan] Kegiatan )"; ?>
                        </option>";
                        <?php } ?>
                    </select>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row example-screen">
	<div class="col-sm-12">
        <p>
            <a href="#" onclick="window.print();"><button class="btn btn-sm btn-default panel-title" style=""><span class="glyphicon glyphicon-print"></span> Cetak</button></a>
            <a href="#openModallaporan"><button class="btn btn-sm btn-default panel-title"><span class="glyphicon glyphicon-plus"></span> Data Laporan Kegiatan</button></a>
            <a href="#openModalperingatan"><button class="btn btn-sm btn-default panel-title"><span class="glyphicon glyphicon-off"></span> Berikan Peringatan Telat</button></a>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading" 
                style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                background-attachment: fixed;">
                <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Daftar Laporan</h4>
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
    	<?php if($ketemu_judul==0){ ?>
            <center><button class="btn btn-sm btn-default panel-title" style="color:#FF0000; margin-bottom:5px;">
            	<span class="glyphicon glyphicon-asterisk"></span> Judul Laporan Kegiatan Tidak Ditemukan <span class="glyphicon glyphicon-asterisk"></span>
            </button></center>
        <?php } ?>
    	<div class="table-responsive-vertical shadow-z-1" style="padding-top:5px;">
		<?php
		if($_GET['filter']=='pusat'){
			$daftar_pusat_tampil = mysql_query("SELECT * FROM datin_laporan a, pusat b WHERE a.id_pusat=b.id_pusat AND a.id_pusat=$_GET[id_pusat] GROUP BY a.tahun");
		}elseif($query_judul==1){
			$daftar_pusat_tampil = mysql_query("SELECT * FROM datin_laporan a, pusat b WHERE a.id_pusat=b.id_pusat AND a.judul_laporan LIKE '%$jlcek%'");
		}else{
			$daftar_pusat_tampil = mysql_query("SELECT * FROM pusat WHERE id_pusat != 1 ORDER BY id_pusat ASC");
		}
        while($tp=mysql_fetch_array($daftar_pusat_tampil)){
        ?>
        
        <h4 style="text-align:center; text-transform:uppercase; line-height: 150%;">
            <?php echo $tp['nama_pusat']; ?>
            <br /> TAHUN ANGGARAN 
			<?php
			if($_GET['filter']=='pusat'){ echo $tp['tahun']; $tahun_nyayeuh=$tp['tahun']; }else{ echo $tahun_kegiatan; $tahun_nyayeuh=$tahun_kegiatan; }
			$dkt = mysql_fetch_array(mysql_query("SELECT count(tahun) as jumlah_laporan FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND type != 'bab' AND tahun=$tahun_nyayeuh"));
			?>
            <br /> <span style="font-size:12px; float:right; margin-right:15px;"><b><i>( <?php echo "$dkt[jumlah_laporan] Kegiatan"; ?> )</i></b></span>
        </h4>
        
          <!-- Table starts here -->
          <table id="table" class="table table-hover table-bordered" style="margin-bottom:100px;">
              <thead style="text-transform:uppercase;">
                  <tr>
                    <th rowspan="2" width="1%">No</th>
                    <th rowspan="2" width="1%"></th>
                    <th rowspan="2" width="15%">Jenis Kegiatan</th>
                    <th rowspan="2">Akronim</th>
                    <th rowspan="2">Ketua / Sekretaris Tim</th>
                    <th colspan="2">Keterangan</th>
                    <th colspan="2" class="example-screen" width="1%">Aksi</th>
                  </tr>
                  <tr>
                    <th width="15%">Softcopy</th>
                    <th  width="15%">Data Penyerahan Hardcopy</th>
                    <th class="example-screen">Edit</th>
                    <th class="example-screen">Hapus</th>
                  </tr>
              </thead>
              <tbody>
              	<?php
				if($query_judul==1){
					$tampil = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat = $tp[id_pusat] AND judul_laporan LIKE '%$jlcek%' ORDER BY id_laporan ASC");
				}
				elseif($_GET['filter']=='pusat'){
					$tampil = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND tahun=$tp[tahun] AND id_laporan_parent=0 ORDER BY id_laporan ASC");
				}
				elseif($_GET['filter']=='tahun'){
					$tampil = mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$tp[id_pusat] AND tahun=$_GET[tahun] AND id_laporan_parent=0 ORDER BY id_laporan ASC");
				}
				else{
					$tampil = mysql_query("SELECT * FROM datin_laporan WHERE tahun=$tahun_kegiatan AND id_pusat=$tp[id_pusat] AND id_laporan_parent=0 ORDER BY id_laporan ASC");
				}

				$no=1;
				while($r=mysql_fetch_array($tampil)){?>
					<?php
                    if($r[type]=='bab'){?>
						<tr align="center">
                          <td data-title="No"><b>									<?php echo $no; ?>						</b></td>
                          <td align="left" data-title="Kegiatan" colspan="6"><b>	<?php echo $r['judul_laporan']; ?> :	</b></td>
                          <td align="left" data-title="Edit Laporan" class="example-screen text-center">
                                <?php echo "<a href='?page=rekapitulasi&act=edit&type=bab&id=$r[id_laporan]' title='Edit $r[judul_laporan]'>
                                            <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit $r[judul_laporan]'></a>"; ?>
                          </td>
                          <td align="left" data-title="Hapus Laporan" class="example-screen text-center">
                                <?php echo "<a href=\"?page=rekapitulasi&act=hapus&id=$r[id_laporan]\"onClick=\"return confirm ('Anda Yakin akan menghapus $r[judul_laporan]')\">
                                            <img src=../images/delete.png alt=hapus class='icon_hareup' border=0 title='Delete $r[judul_laporan]'></a>"; ?>
                          </td>
                        </tr>
                        <?php
						$cek_laporan_parent = mysql_query("SELECT * FROM datin_laporan WHERE id_laporan_parent=$r[id_laporan] ORDER BY id_laporan ASC");
						$cekjml=mysql_num_rows($cek_laporan_parent);
						if($cekjml!=0){
							$no1=1;
							while($lparent2=mysql_fetch_array($cek_laporan_parent)){?>
								<?php
                                if($lparent2[type]=='bab'){?>
                                    <tr align="center">
                                      <td></td>
                                      <td data-title="No">									<?php echo $no1; ?>).						</td>
                                      <td align="left" data-title="Kegiatan" colspan="5">	<?php echo $lparent2['judul_laporan']; ?> :		</td>
                                      <td align="left" data-title="Edit Laporan" class="example-screen text-center">
                                            <?php echo "<a href='?page=rekapitulasi&act=edit&type=bab&id=$lparent2[id_laporan]' title='Edit $lparent2[judul_laporan]'>
                                                        <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit $lparent2[judul_laporan]'></a>"; ?>
                                      </td>
                                      <td align="left" data-title="Hapus Laporan" class="example-screen text-center">
                                            <?php echo "<a href=\"?page=rekapitulasi&act=hapus&id=$lparent2[id_laporan]\"onClick=\"return confirm ('Anda Yakin akan menghapus $lparent2[judul_laporan]')\">
                                                        <img src=../images/delete.png alt=hapus class='icon_hareup' border=0 title='Delete $lparent2[judul_laporan]'></a>"; ?>
                                      </td>
                                    </tr>
                                    <?php
                                    $cek_laporan_parent2 = mysql_query("SELECT * FROM datin_laporan WHERE id_laporan_parent=$lparent2[id_laporan] ORDER BY id_laporan ASC");
                                    $cekjml2=mysql_num_rows($cek_laporan_parent2);
                                    if($cekjml2!=0){
                                        $no2='a';
                                        while($lparent3=mysql_fetch_array($cek_laporan_parent2)){?>
											<?php
                                            if($lparent3[type]=='bab'){?>
                                                <tr align="center">
                                                  <td></td>
                                                  <td data-title="No">									<?php echo $no2; ?>	).					</td>
                                                  <td align="left" data-title="Kegiatan" colspan="5">	<?php echo $lparent3['judul_laporan']; ?> :		</td>
                                                  <td align="left" data-title="Edit Laporan" class="example-screen text-center">
                                                        <?php echo "<a href='?page=rekapitulasi&act=edit&type=bab&id=$lparent3[id_laporan]' title='Edit $lparent3[judul_laporan]'>
                                                                    <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit $lparent3[judul_laporan]'></a>"; ?>
                                                  </td>
                                                  <td align="left" data-title="Hapus Laporan" class="example-screen text-center">
                                                        <?php echo "<a href=\"?page=rekapitulasi&act=hapus&id=$lparent3[id_laporan]\"onClick=\"return confirm ('Anda Yakin akan menghapus $lparent3[judul_laporan]')\">
                                                                    <img src=../images/delete.png alt=hapus class='icon_hareup' border=0 title='Delete $lparent3[judul_laporan]'></a>"; ?>
                                                  </td>
                                                </tr>
                                                <?php
                                                $cek_laporan_parent3 = mysql_query("SELECT * FROM datin_laporan WHERE id_laporan_parent=$lparent3[id_laporan] ORDER BY id_laporan ASC");
                                                $cekjml3=mysql_num_rows($cek_laporan_parent3);
                                                if($cekjml3!=0){
                                                    $no3=1;
                                                    while($lparent4=mysql_fetch_array($cek_laporan_parent3)){
                                                        
                                                    }
                                                }//tutup if paren3
                                                ?>
                                            <?php
                                            }// tutup if daftar bab
                                            else{
                                            ?>
                                                <?php
                                                $ketua_sek3=explode("/",$lparent3['ketua_sek']);
                                                $tgl_upload3=tgl_indo($lparent3[tgl_upload]);
                                                $tgl_penyerahan3=tgl_indo($lparent3[tgl_hardcopy]);
                                                ?>
                                                <tr align="center">
                                                  <td></td>
                                                  <td data-title="No">							<?php echo $no2; ?>).						</td>
                                                  <td align="left" data-title="Kegiatan">		<?php echo $lparent3['judul_laporan']; ?>	</td>
                                                  <td data-title="Akronim">						<?php echo $lparent3['akronim']; ?>			</td>
                                                  <td data-title="Ketua/Sekretaris">			
                                                        <?php 	if(!empty($lparent3['ketua_sek'])){
                                                                    if(empty($ketua_sek3[1])) { echo "Ketua :<br>$ketua_sek3[0]"; }
																	elseif(empty($ketua_sek3[0])) { echo "Sekretaris :<br>$ketua_sek3[1]"; } 
                                                                    else { echo "Ketua :<br> ".$ketua_sek3[0]." <br><br> Sekretaris :<br> ".$ketua_sek3[1].""; } 
                                                                }
                                                        ?>
                                                  </td>
                                                  <td data-title="Softcopy">
                                                        <?php if(!empty($lparent3['file_fulltext'])){ ?>
                                                            Nama File : <br /><a href="file_laporan/<?php echo $lparent3['file_fulltext']; ?>" target="_blank" class="example-screen">
																				<?php echo $lparent3['file_fulltext']; ?> </a>
                                                                                <span class="example-print"><?php echo $lparent3['file_fulltext']; ?></span>
                                                                  <br /><br />
                                                            Tanggal Upload : <br /><?php echo $tgl_upload3; ?>
                                                        <?php } ?>
                                                  </td>
                                                  <td data-title="Hardcopy">
                                                        <?php if(!empty($lparent3['eksemplar'])){ ?>
                                                            Jumlah : <br /><?php echo $lparent3['eksemplar']; ?> Eksemplar<br /><br />
                                                            Tanggal Penyerahan : <br /><?php echo $tgl_penyerahan3 ?>
                                                        <?php } ?>
                                                  </td>
                                                  <td align="left" data-title="Edit Laporan" class="example-screen text-center">
                                                        <?php echo "<a href='?page=rekapitulasi&act=edit&id=$lparent3[id_laporan]' title='Edit $lparent3[judul_laporan]'>
                                                                    <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit $lparent3[judul_laporan]'></a>"; ?>
                                                  </td>
                                                  <td align="left" data-title="Hapus Laporan" class="example-screen text-center">
                                                        <?php echo "<a href=\"?page=rekapitulasi&act=hapus&id=$lparent3[id_laporan]\"onClick=\"return confirm ('Anda Yakin akan menghapus $lparent3[judul_laporan]')\">
                                                                    <img src=../images/delete.png alt=hapus class='icon_hareup' border=0 title='Delete $lparent3[judul_laporan]'></a>"; ?>
                                                  </td>
                                                </tr>
                                            <?php 
                                            }// tutup else daftar laporan
                                            ?>
                                        <?php $no2++;
                                        } // tutup while daftar laporan
                                    }//tutup if paren2
                                    ?>
                                <?php
                                }// tutup if daftar bab
                                else{
                                ?>
                                    <?php
                                    $ketua_sek2=explode("/",$lparent2['ketua_sek']);
                                    $tgl_upload2=tgl_indo($lparent2[tgl_upload]);
                                    $tgl_penyerahan2=tgl_indo($lparent2[tgl_hardcopy]);
                                    ?>
                                    <tr align="center">
                                      <td></td>
                                      <td data-title="No">							<?php echo $no1; ?>).						</td>
                                      <td align="left" data-title="Kegiatan"> 		<?php echo $lparent2['judul_laporan']; ?>	</td>
                                      <td data-title="Akronim">						<?php echo $lparent2['akronim']; ?>			</td>
                                      <td data-title="Ketua/Sekretaris">			
                                            <?php 	if(!empty($lparent2['ketua_sek'])){
                                                        if(empty($ketua_sek2[1])) { echo "Ketua :<br>$ketua_sek2[0]"; } elseif(empty($ketua_sek2[0])) { echo "Sekretaris :<br>$ketua_sek2[1]"; } 
                                                        else { echo "Ketua :<br> ".$ketua_sek2[0]." <br><br> Sekretaris :<br> ".$ketua_sek2[1].""; } 
                                                    }
                                            ?>
                                      </td>
                                      <td data-title="Softcopy">
                                            <?php if(!empty($lparent2['file_fulltext'])){ ?>
                                                Nama File : <br /><a href="file_laporan/<?php echo $lparent2['file_fulltext']; ?>" target="_blank" class="example-screen">
																				<?php echo $lparent2['file_fulltext']; ?> </a>
                                                                                <span class="example-print"><?php echo $lparent2['file_fulltext']; ?></span>
                                                                  <br /><br />
                                                Tanggal Upload : <br /><?php echo $tgl_upload2; ?>
                                            <?php } ?>
                                      </td>
                                      <td data-title="Hardcopy">
                                            <?php if(!empty($lparent2['eksemplar'])){ ?>
                                                Jumlah : <br /><?php echo $lparent2['eksemplar']; ?> Eksemplar<br /><br />
                                                Tanggal Penyerahan : <br /><?php echo $tgl_penyerahan2 ?>
                                            <?php } ?>
                                      </td>
                                      <td align="left" data-title="Edit Laporan" class="example-screen text-center">
                                            <?php echo "<a href='?page=rekapitulasi&act=edit&id=$lparent2[id_laporan]' title='Edit $lparent2[judul_laporan]'>
                                                        <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit $lparent2[judul_laporan]'></a>"; ?>
                                      </td>
                                      <td align="left" data-title="Hapus Laporan" class="example-screen text-center">
                                            <?php echo "<a href=\"?page=rekapitulasi&act=hapus&id=$lparent2[id_laporan]\"onClick=\"return confirm ('Anda Yakin akan menghapus $lparent2[judul_laporan]')\">
                                                        <img src=../images/delete.png alt=hapus class='icon_hareup' border=0 title='Delete $lparent2[judul_laporan]'></a>"; ?>
                                      </td>
                                    </tr>
                                <?php 
                                }// tutup else daftar laporan
                                ?>
                            <?php $no1++;
                            } // tutup while daftar laporan
						}//tutup if parent1
						?>
                    <?php
                    }// tutup if daftar bab
                    else{
                    ?>
                        <?php
                        $ketua_sek=explode("/",$r['ketua_sek']);
                        $tgl_upload=tgl_indo($r[tgl_upload]);
                        $tgl_penyerahan=tgl_indo($r[tgl_hardcopy]);
                        ?>
                        <tr align="center">
                          <td data-title="No">	<b>						<?php echo $no; ?>						</b></td>
                          <td align="left" data-title="Kegiatan" style="text-transform:uppercase;" colspan="2"><b>	<?php echo $r['judul_laporan']; ?>		</b></td>
                          <td data-title="Akronim">						<?php echo $r['akronim']; ?>			</td>
                          <td data-title="Ketua/Sekretaris">			
                                <?php 	if(!empty($r['ketua_sek'])){
                                            if(empty($ketua_sek[1])) { echo "Ketua :<br>$ketua_sek[0]"; } elseif(empty($ketua_sek[0])) { echo "Sekretaris :<br>$ketua_sek[1]"; } 
                                            else { echo "Ketua :<br> ".$ketua_sek[0]." <br><br> Sekretaris :<br> ".$ketua_sek[1].""; } 
                                        }
                                ?>
                          </td>
                          <td data-title="Softcopy">
                                <?php if(!empty($r['file_fulltext'])){ ?>
                                    Nama File : <br /><a href="file_laporan/<?php echo $r['file_fulltext']; ?>" target="_blank" class="example-screen">
																				<?php echo $r['file_fulltext']; ?> </a>
                                                                                <span class="example-print"><?php echo $r['file_fulltext']; ?></span>
                                                                  <br /><br />
                                    Tanggal Upload : <br /><?php echo $tgl_upload; ?>
                                <?php } ?>
                          </td>
                          <td data-title="Hardcopy">
                                <?php if(!empty($r['eksemplar'])){ ?>
                                    Jumlah : <br /><?php echo $r['eksemplar']; ?> Eksemplar<br /><br />
                                    Tanggal Penyerahan : <br /><?php echo $tgl_penyerahan ?>
                                <?php } ?>
                          </td>
                          <td align="left" data-title="Edit Laporan" class="example-screen text-center">
                                <?php echo "<a href='?page=rekapitulasi&act=edit&id=$r[id_laporan]' title='Edit $r[judul_laporan]'>
                                            <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit $r[judul_laporan]'></a>"; ?>
                          </td>
                          <td align="left" data-title="Hapus Laporan" class="example-screen text-center">
                                <?php echo "<a href=\"?page=rekapitulasi&act=hapus&id=$r[id_laporan]\"onClick=\"return confirm ('Anda Yakin akan menghapus $r[judul_laporan]')\">
                                            <img src=../images/delete.png alt=hapus class='icon_hareup' border=0 title='Delete $r[judul_laporan]'></a>"; ?>
                          </td>
                        </tr>
                    <?php 
                    }// tutup else daftar laporan
                    ?>
                <?php $no++;
				} // tutup while daftar laporan?>
              </tbody>
            </table>
            <div class="example-print" style="margin-bottom:1000px;"></div>
          <?php } // tutup while Pusat Eselon II?>
            
    	</div>
    </div>
</div>
<?php
break;
//==========================================================================================================================
case "tambah";
if($_POST[type]=='bab'){
	$judul_bab= strtoupper($_POST['judul_laporan']);
	mysql_query("INSERT INTO datin_laporan(id_user, id_laporan_parent, id_pusat, judul_laporan, tahun, type, tgl_post) 
						VALUES('$_SESSION[id_user]', '$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$judul_bab', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang')");
	echo "<script language='JavaScript'>location.href='?page=rekapitulasi';</script>";
}else{
	mysql_query("INSERT INTO datin_laporan(id_user, id_laporan_parent, id_pusat, judul_laporan, tahun, akronim, ketua_sek, tgl_post) 
						VALUES('$_SESSION[id_user]', '$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', '$_POST[akronim]', '$_POST[ketua_sek]', '$tgl_sekarang')");
	echo "<script language='JavaScript'>location.href='?page=rekapitulasi';</script>";
}

//echo"'$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";

break;
//=============================================================================================================================
case"edit";
$edit = mysql_query("SELECT * FROM datin_laporan a, pusat b WHERE a.id_pusat=b.id_pusat AND a.id_laporan = $_GET[id]");
$r= mysql_fetch_array($edit);
$tgl_hardcopy=tgl_indo($r[tgl_hardcopy]);
?>
<form method='POST' action='?page=rekapitulasi&act=editnya'  enctype='multipart/form-data'>
	<input type='hidden' name='id' value='<?php echo $r['id_laporan']; ?>'>
    <div class="panel panel-default">
        <div class="panel-heading" 
            style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
            background-attachment: fixed;">
            <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Perubahan Data Laporan Kegiatan</h4>
        </div>
        <div class="panel-body">
            <div class="row" style="margin-bottom:20px;">
                <div class="col-sm-2">
                    Tipe Laporan
                    <select class="form-control" name="type">
                        <option value="">Laporan</option>
                        <option value="bab" <?php if($r['type']=='bab'){?> selected="selected" <?php } ?>>Kepala Kegiatan</option>
                    </select> 
                </div>
                <div class="col-sm-4">
                    Pilih Eselon II
                    <select class="form-control pusat" name="id_pusat" >
                        <?php
                        $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat > 1 ORDER BY id_pusat ASC");
                        while ($p=mysql_fetch_array($daftar_pusat)){ 
							if ($r[id_pusat]==$p[id_pusat]){
								echo "<option value=$p[id_pusat] selected>$p[nama_pusat]</option>";
							}
							else{
								echo "<option value=$p[id_pusat]>$p[nama_pusat]</option>";
							}
                         } ?>
                    </select> 
                </div>
                <div class="col-sm-6">
             <?php $elp = mysql_fetch_array(mysql_query("SELECT * FROM datin_laporan WHERE id_laporan=$r[id_laporan_parent]")); ?>
                    Pilih Jenis Kepala Kegiatan
                    <select class="form-control laporan_parent" name="id_laporan_parent">
                        <?php if($r['id_laporan_parent'] != 0) {  ?>
								<option selected="selected" value="<?php echo $r['id_laporan_parent'];?>"><?php echo $elp['judul_laporan'];?></option>
						<?php } else { ?>
                        		<option selected="selected"> -- Pilih Kepala Kegiatan -- </option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="row" style="margin-bottom:20px;">
                <div class="col-sm-1">
                    Tahun
                    <input name='tahun' class="form-control" type='text' placeholder='Tahun' required="" onKeyUp="validAngka(this)" value="<?php echo $r['tahun']; ?>" />
                </div>
                <div class="col-sm-6">
                    Judul Kegiatan
                    <input name='judul_laporan' class="form-control" type='text' placeholder='Judul Laporan' required="" value="<?php echo $r['judul_laporan']; ?>"/>
                </div>
                <div class="col-sm-2">
                    Akronim
                    <input name='akronim' class="form-control" type='text' placeholder='Akronim Laporan' value="<?php echo $r['akronim']; ?>"/>
                </div>
                <div class="col-sm-3">
                    Ketua / Sekretaris
                    <input name='ketua_sek' class="form-control" type='text' placeholder='Ketua / Sekretaris' value="<?php echo $r['ketua_sek']; ?>"/>
                </div>
           </div>
           <div class="row">
           		<div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Data Softcopy <span style="font-size:10px;"></span></h4>
                        </div>
                        <div class="panel-body">
                        	<div class="row">
                                <div class="col-sm-12">
                                	<input type='file' name='fupload' class="form-control" style="padding-bottom:40px;">
                            	</div>
                                 <div class="col-sm-12" style="margin-top:10px;">
                                	<p><?php if(!empty($r['file_fulltext'])){?> Nama File : <?php echo $r['file_fulltext']; ?><?php 
                                             }else{ ?>Belum ada Fulltext laporan kegiatan <?php } ?>
                                    </p>
                            	</div>
                            </div>
                        </div>
                    </div>
                </div>

           		<div class="col-sm-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Data Hardcopy</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="row">
                                <div class="col-sm-5">
                                    Eksemplar
                                    <input name='eksemplar' class="form-control" type='text' placeholder='Eksemplar' 
                                    value="<?php if($r['eksemplar'] != 0) { echo $r['eksemplar']; }?>"/>
                                </div>
                                <div class="col-sm-7">
                                    Tanggal Penyerahan
                                    <input name='tgl_hardcopy' class="form-control" type='text' placeholder='Thn-Bln-Tgl<?php //echo date("Y-m-d"); ?>' 
                                    value="<?php if($r['tgl_hardcopy'] != 0) { echo $r['tgl_hardcopy']; }?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
           	<!--<div class="col-sm-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Keterangan</h4>
                        </div>
                        <div class="panel-body">
                        	<textarea name='ketua_sek' class="form-control" type='text' placeholder='Ex : <?php echo $tgl_sekarang; ?>' value="<?php echo $r['ketua_sek']; ?>"/> </textarea>   
                        </div>
                    </div>
                </div>
           </div>
           <div class="row">-->
                <div class="col-sm-1" style="text-align:center;"> &nbsp;
                    <button class="btn btn-default" type="submit" style="width:100%;">
                        <span class="glyphicon glyphicon-save"></span> Simpan
                    </button>
                    <a href="?page=rekapitulasi">
                    <span class="btn btn-default" style="width:100%; margin-top:10px;">
                        <span class="glyphicon glyphicon-step-backward"></span> Kembali
                    </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
break;
//=============================================================================================================================
case "editnya";
if($_POST[type]=='bab'){
	$judul_bab= strtoupper($_POST['judul_laporan']);
	mysql_query("UPDATE datin_laporan SET 	id_laporan_parent  = '$_POST[id_laporan_parent]', id_pusat  = '$_POST[id_pusat]', judul_laporan = '$judul_bab', 
											tahun = '$_POST[tahun]', type = '$_POST[type]'
				  WHERE id_laporan     = '$_POST[id]'");
	
	echo "<script language='JavaScript'>location.href='?page=rekapitulasi&act=edit&type=bab&id=$_POST[id]';</script>";
}else{
	$lokasi_file    = $_FILES['fupload']['tmp_name'];
	$tipe_file      = $_FILES['fupload']['type'];
	$nama_file      = $_FILES['fupload']['name'];
	if(!empty($lokasi_file)){
	move_uploaded_file($lokasi_file,"file_laporan/$nama_file");
	mysql_query("UPDATE datin_laporan SET id_laporan_parent  = '$_POST[id_laporan_parent]', id_pusat  = '$_POST[id_pusat]', judul_laporan = '$_POST[judul_laporan]', 
										tahun = '$_POST[tahun]', type = '$_POST[type]',
										  akronim  = '$_POST[akronim]', ketua_sek  = '$_POST[ketua_sek]', eksemplar = '$_POST[eksemplar]', tgl_hardcopy = '$_POST[tgl_hardcopy]',
										  file_fulltext = '$nama_file', tgl_upload  = '$tgl_sekarang', pending = 1, approve = 1, tgl_approve = '$tgl_sekarang'
				  WHERE id_laporan     = '$_POST[id]'");
	}
	else{
	mysql_query("UPDATE datin_laporan SET id_laporan_parent  = '$_POST[id_laporan_parent]', id_pusat  = '$_POST[id_pusat]', judul_laporan = '$_POST[judul_laporan]',
											tahun = '$_POST[tahun]', type = '$_POST[type]',
										  akronim  = '$_POST[akronim]', ketua_sek  = '$_POST[ketua_sek]', eksemplar = '$_POST[eksemplar]', tgl_hardcopy = '$_POST[tgl_hardcopy]'
				  WHERE id_laporan     = '$_POST[id]'");
	}
	echo "<script language='JavaScript'>location.href='?page=rekapitulasi&act=edit&id=$_POST[id]';</script>";
}

//echo"'$_POST[id_laporan_parent]', '$_POST[id_pusat]', '$_POST[judul_laporan]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";

break;
//================================================================================================================================
case "hapus";
mysql_query("DELETE FROM datin_laporan WHERE id_laporan='$_GET[id]'");

echo "<script language='JavaScript'>location.href='?page=rekapitulasi';</script>";

break;
//===================================================================================================================================
}
?>
