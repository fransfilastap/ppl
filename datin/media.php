<?php
include "../config/koneksi.php";
include "../config/library.php";
include "../config/fungsi_indotgl.php";
include "../config/fungsi_combobox1.php";
include "../config/class_paging.php";
include "../config/rupiah.php";

session_start();
if (empty($_SESSION[id_user])){
	echo "<script language='JavaScript'> location.href='../login.php?login=login'; </script>";
}
else{
 
	if($_SESSION['type_user']=='admin'){
		$query=mysql_query("SELECT * FROM datin_laporan WHERE type != 'bab' AND approve=0 AND file_fulltext is NULL");
	}else{
		$query=mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$_SESSION[id_pusat] AND approve=0 AND type != 'bab'");
	}
	$jl= mysql_num_rows($query);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Aplikasi Datin - PPL BPHN</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Bagian Program dan Pelaporan">

		<!-- icons -->
		<link rel="shortcut icon" href="http://www.bphn.go.id/public/bphn/images/icon.png">

		<!-- Bootstrap Core CSS file -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">

		<!-- Override CSS file - add your own CSS rules -->
		<link rel="stylesheet" href="assets/css/styles.css">
        
        <!-- Auto Complete-->
        <link rel="stylesheet" href="../config/autocomplete/awesomplete.css" />
		<script src="../config/autocomplete/awesomplete.js"></script>
        <!-- Auto Complete -->
        
        <!-- Tools Tip -->
        <link rel="stylesheet" href="../config/toolstip/tooltip.css" />
        <script type='text/javascript' src='../config/toolstip/jquery.min.js'></script>
        <script type='text/javascript' src='../config/toolstip/touchHover.js'></script>
        <!-- Tools Tip -->
        
        <!-- style text -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Cinzel:900">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Exo">
        <!-- style text -->
        
        <script language='javascript'>
		function validAngka(a)
		{
			if(!/^[0-9.]+$/.test(a.value))
			{
			a.value = a.value.substring(0,a.value.length-1000);
			}
		}
		</script>
        
        <!-- javascript pusat ===> laporan_parent-->
        <script type="text/javascript" src="../config/js/jquery-1.4.1.min.js"></script>
        <!-- javascript pusat ===> laporan_parent-->
        
	</head>
	<body>

		<!-- Navigation -->
	    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
			<div class="container-fluid">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand logo" href="#">DATIN</a>
				</div>
				<!-- /.navbar-header -->     

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav" style="font-family:'Exo', sans-serif; text-transform:uppercase;">
						<li><a href="?page=dashboard"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
						<?php if($_SESSION[type_user]=='admin'){ ?>
                        <li><a href="?page=rekapitulasi"><span class="glyphicon glyphicon-open-file"></span> Rekapitulasi Kegiatan</a></li> 
						<?php } ?>
                        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
            
            <!-- Bagian Slide Side -->
            <div id="mySidenav" class="sidenav">
              <a href="javascript:void(0)" class="closebtn" onClick="closeNav()">&times;</a>
              	<div class="row col-sm-12" style="margin-top:10px;">
                      <div class="col-sm-3">
                          <a href="../datin/">
                              <div class="titlebox icon_hareup">Pelaporan Hasil Kegitan</div>
                              <div class="box" style="text-align:center;"><?php if($jl!=0){echo "<notif>".$jl."</notif>"; }?>
                                 <img src="../landingpage/datin.png" class="icon_hareup">                  
                              </div>
                          </a>
                       </div>
                       
                       <div class="col-sm-3">
                          <a href="../realisasi/">
                              <div class="titlebox icon_hareup">Pelaporan Kinerja</div>
                              <div class="box" style="text-align:center;">
                                 <img src="../landingpage/lakip.png" class="icon_hareup">                  
                              </div>
                          </a>
                       </div>
                       
                       <div class="col-sm-3">
                          <a href="../e-disposisi/">
                              <div class="titlebox icon_hareup">E-Disposisi</div>
                              <div class="box" style="text-align:center;">
                                 <img src="../landingpage/e-disposisi.png" class="icon_hareup">                  
                              </div>
                          </a>
                       </div>
                </div>
            </div>
            
            <span id="menuopen" onClick="openNav()">
            	<img src="assets/img/open.png" class="icon_hareup" style="max-height:50px;">
                <!--<span class="glyphicon glyphicon-object-align-right icon_hareup menuslide"></span>-->
            </span>
            
            <script>
				function openNav() { document.getElementById("mySidenav").style.width = "100%"; }
				function closeNav(){ document.getElementById("mySidenav").style.width = "0"; }
            </script>
            <!-- Bagian Slide Side -->
            
		</nav>
		<!-- /.navbar -->
        
        

		<!-- Page Content -->
		<div class="container-fluid">
			<!--<div class="row">
				<div class="col-sm-12">
					<div class="page-header">
						<h1>Blog post title</h1>
						<p>Posted by <span class="glyphicon glyphicon-user"></span> <a href="#">Matthijs Jansen</a> on <span class="glyphicon glyphicon-time"></span> 12 January 2015 10:00 am</p>
					</div>
				</div>
			</div>-->


			<!-- /.row -->

			<div class="row">
				<div class="col-sm-12">
                <?php
					if ($_GET[page]=='dashboard'){
						include "content/dashboard.php";
					}
					elseif ($_GET[page]=='rekapitulasi'){
						include "content/rekapitulasi.php";
					}
					else{
						echo "<script language='JavaScript'>
						   location.href='?page=dashboard';
						   </script>";		
					}
				?>
                </div>
			</div>
			<!-- /.row -->

			<hr>
			<footer>
				<div class="row example-screen">
					<div class="col-lg-12">
						<p>Copyright &copy; Bagian Program dan Pelaporan 2017</p>
					</div>
				</div>
			</footer>
		</div>
		<!-- /.container-fluid --> 
        
        <!-- Back To Top -->
        <a id="backtotop" href="#top" title="Kembali Keatas"><span class="glyphicon glyphicon-triangle-top"></span></a> 
        <script src="../config/scripts/jquery.backtotop.js"></script>       
        <!-- Back To Top -->

		<!-- JQuery scripts -->
	    <script src="assets/js/jquery-1.11.2.min.js"></script>

		<!-- Bootstrap Core scripts -->
		<script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>
<?php } ?>