<script type="text/javascript">
$(document).ready(function(){
	$(".realisasi").change(function()
	{
		var id=$(this).val();
		var dataString = 'id='+ id;
	
		$.ajax
		({
			type: "POST",
			url: "get_kegiatan.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".kegiatan_parent").html(html);
			} 
		});
	});
	$(".realisasi_data").change(function()
	{
		var id=$(this).val();
		var dataString = 'id='+ id;
	
		$.ajax
		({
			type: "POST",
			url: "get_kegiatan_sub.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".kegiatan_parent_sub").html(html);
			} 
		});
	});	
});
</script>
<?php
session_start();
//$tahun_kegiatan = date("Y");
if(!empty($_POST[tahun])){ $tahun_kegiatan = $_POST[tahun]; } else{ $tahun_kegiatan = $_GET[tahun]; }
if(!empty($_POST[id_pusat])){ $id_pusat_kegiatan = $_POST[id_pusat]; } else{ $id_pusat_kegiatan = $_SESSION[id_pusat]; }
if(!empty($_GET[bulan])){ $bln_kegiatan = $_GET[bulan];  } else{ $bln_kegiatan = date("m"); }
$blnnyayeuh =  getbulan($bln_kegiatan);

$_SESSION[tahun]=$tahun_kegiatan; $_SESSION[bulan]=$bln_kegiatan;
switch($_GET[act]){
  // Tampil contact
default:
?> 
<div class="row example-print">
	<div class="col-sm-12">
    	<h1 style="text-align:center; text-transform:uppercase; font-size:34px;">
        RENCANA KERJA TAHUNAN DAN <br /> REALISASI KINERJA & ANGGARAN <br /> BADAN PEMBINAAN HUKUM NASIONAL <br /> KEMENTERIAN HUKUM DAN HAM RI 
        <br /> TAHUN ANGGARAN <?php echo $tahun_kegiatan; ?><br /><br />
        <img src="assets/img/logo_bphn.png"  style="height:350px;">
		</h1>
    </div>
</div>

<div id="openModalindikatorJudul" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Penambahan Judul Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=renjabulanantu&act=tambah' enctype='multipart/form-data'>
                        	<input type='hidden' name='tipe' value='bab'>
                        	<div class="row">
                                <div class="col-sm-12">
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-sm-12">&nbsp;
                                            <select class="form-control realisasi" name="id_realisasi" >
                                                <option selected="selected">-- Pilih Indikator Kinerja --</option>
                                                <?php
                                                $daftar_indikator = mysql_query("SELECT * FROM realisasi_sasaran WHERE tahun=$tahun_kegiatan AND id_pusat=$id_pusat_kegiatan AND id_realisasi_parent != 0 
																				 ORDER BY id_realisasi ASC");
                                                while ($i=mysql_fetch_array($daftar_indikator)){ 
                                                ?>
                                                <option value="<?php echo $i['id_realisasi'];?>"> <?php echo $i['judul_realisasi']; ?> </option>
                                                <?php } ?>
                                            </select> 
                                        </div>
                                        <div class="col-sm-12" style="margin-bottom:20px;">&nbsp;
                                            <select class="form-control kegiatan_parent" name="id_kegiatan_parent">
                                                <option selected="selected">-- Pilih Kepala Kegiatan --</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            Tipe Kegiatan
                                            <select class="form-control" name="type">
                                                <option value=""  selected="selected">Data Kegiatan</option>
                                                <option value="bab">Kepala Kegiatan</option>
                                            </select> 
                                        </div>
                                        <div class="col-sm-9">
                                            Judul Kegiatan
                                            <input name='judul_kegiatan' class="form-control" type='text' placeholder='Judul Kegiatan' required=""/>
                                        </div> 
                                    </div>
                                </div><!-- tutup col12-->
                         </div>
                         <div class="row">
                            <div class="col-sm-12" style="text-align:center;">
                                    <button class="btn btn-default" type="submit" style="width:100%;">
                                        <span class="glyphicon glyphicon-save"></span> Simpan
                                    </button>
                            </div>
                         </div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<div id="openModaltahun" class="modalDialog">
    <div align="center"><!--<a href="#close" title="Close" class="close">&times;</a>-->
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Pilih Eselon II dan Tahun Kegiatannya</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=renjabulanantu'>
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-sm-6">                               
                                    <?php
                                    $daftar_pusat = mysql_query("SELECT * FROM pusat WHERE id_pusat=$id_pusat_kegiatan");
                                    $p=mysql_fetch_array($daftar_pusat); 
                                    ?>
                                    <br /><input class="form-control" type='text' disabled="disabled" placeholder="<?php echo $p['nama_pusat']; ?>"/>
                            </div>
                            <div class="col-sm-2">
                                Tahun Kegiatan
                                <input name='tahun' class="form-control" type='text' placeholder='Tahun Kegiatan' required="" onKeyUp="validAngka(this)" />
                            </div>
                            <div class="col-sm-4" style="text-align:center;">&nbsp;
                                    <button class="btn btn-default" type="submit" style="width:100%;">
                                        <span class="glyphicon glyphicon-search"></span> Cari
                                    </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<div id="openModalindikator" class="modalDialog">
    <div align="center"><a href="#close" title="Close" class="close">&times;</a>
         <div class="row example-screen" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" 
                        style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
                        background-attachment: fixed;">
                        <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Penambahan Data Realisasi Kinerja dan Anggaran</h4>
                    </div>
                    <div class="panel-body">
                    	<form method='POST' action='?page=renjabulanantu&act=tambah' enctype='multipart/form-data'>
                        	<input type='hidden' name='tahun' value='<?php echo $tahun_kegiatan; ?>'>
                            <input type='hidden' name='bulan' value='<?php echo $bln_kegiatan; ?>'>
                        	<div class="row">
                                <div class="col-sm-8">
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-sm-12">
                                            Pilih Indikator Kinerja
                                            <select class="form-control realisasi_data" name="id_realisasi" >
                                                <option selected="selected">-- Pilih Indikator Kinerja --</option>
                                                <?php
                                                $daftar_indikator = mysql_query("SELECT * FROM realisasi_sasaran WHERE tahun=$tahun_kegiatan AND id_pusat=$id_pusat_kegiatan AND id_realisasi_parent != 0 
																				 ORDER BY id_realisasi ASC");
                                                while ($i=mysql_fetch_array($daftar_indikator)){ 
                                                ?>
                                                <option value="<?php echo $i['id_realisasi'];?>"> <?php echo $i['judul_realisasi']; ?> </option>
                                                <?php } ?>
                                            </select> 
                                        </div>
                                        <!--<div class="col-sm-12">
                                            Pilih Jenis Kepala Kegiatan
                                            <select class="form-control kegiatan_parent" name="id_kegiatan_parent">
                                                <option selected="selected">-- Pilih Kepala Kegiatan --</option>
                                            </select>
                                        </div>-->
                                        <div class="col-sm-12">
                                            Pilih Data Kegiatan
                                            <select class="form-control kegiatan_parent_sub" name="id_kegiatan">
                                                <option selected="selected">-- Pilih Kegiatan --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom:20px;">
                                        <div class="col-sm-3">
                                            Target Kinerja (%)
                                    		<input name='kinerja_target' class="form-control" type='text' placeholder='Ex : 100' required="" onKeyUp="validAngka(this)" />
                                        </div>
                                        <div class="col-sm-3">
                                            Realisasi Kinerja (%)
                                    		<input name='kinerja_realisasi' class="form-control" type='text' placeholder='Ex : 53' required="" onKeyUp="validAngka(this)" />
                                        </div>
                                        <div class="col-sm-3">
                                            Realisasi Anggaran
                                    		<input name='kinerja_anggaran' class="form-control" type='text' placeholder='Ex : 12356789' required="" onKeyUp="validAngka(this)" />
                                        </div>
                                        <div class="col-sm-3">
                                            <u>Upload Data Dukung</u>
                                    		<input type='file' name='fupload' style="margin-top:8px;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Hambatan</h4>
                                                </div>
                                                <div class="panel-body">
                                                        <?php include("../fckeditor.php") ;?>
                                                        <?php 
                                                            $sBasePath = $_SERVER['PHP_SELF'] ;
                                                            $sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "realisasi" ) ) ;
                                                            $oFCKeditor = new FCKeditor('hambatan') ;
                                                            $oFCKeditor->ToolbarSet = 'Basic';
                                                            $oFCKeditor->BasePath	= $sBasePath ;
                                                            $oFCKeditor->Config['SkinPath'] = $sBasePath . 'editor/skins/' .'silver'. '/' ;
                                                            $oFCKeditor->Width = '100%';
                                                            $oFCKeditor->Height = '120';
                                                            $oFCKeditor->Value		= '' ;
                                                            $oFCKeditor->Create() 
                                                        ;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">Solusi</h4>
                                                </div>
                                                <div class="panel-body">
                                                        <?php include("../fckeditor.php") ;?>
                                                        <?php 
                                                            $sBasePath = $_SERVER['PHP_SELF'] ;
                                                            $sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "realisasi" ) ) ;
                                                            $oFCKeditor = new FCKeditor('solusi') ;
                                                            $oFCKeditor->ToolbarSet = 'Basic';
                                                            $oFCKeditor->BasePath	= $sBasePath ;
                                                            $oFCKeditor->Config['SkinPath'] = $sBasePath . 'editor/skins/' .'silver'. '/' ;
                                                            $oFCKeditor->Width = '100%';
                                                            $oFCKeditor->Height = '120';
                                                            $oFCKeditor->Value		= '' ;
                                                            $oFCKeditor->Create() 
                                                        ;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- tutup col8-->
                                
                                <div class="col-sm-4">
                                	<div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Realisasi Target Kinerja & Output Bulan <?php echo $blnnyayeuh; ?></h4>
                                        </div>
                                        <div class="panel-body">
                                                <?php include("../fckeditor.php") ;?>
                                                <?php 
                                                    $sBasePath = $_SERVER['PHP_SELF'] ;
                                                    $sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "realisasi" ) ) ;
                                                    $oFCKeditor = new FCKeditor('output_blnan') ;
                                                    $oFCKeditor->ToolbarSet = 'Basic';
                                                    $oFCKeditor->BasePath	= $sBasePath ;
                                                    $oFCKeditor->Config['SkinPath'] = $sBasePath . 'editor/skins/' .'silver'. '/' ;
                                                    $oFCKeditor->Width = '100%';
                                                    $oFCKeditor->Height = '370';
                                                    $oFCKeditor->Value		= '' ;
                                                    $oFCKeditor->Create() 
                                                ;?>
                                        </div>
                                	</div>
                                </div>
                         </div>
                         <div class="row">
                            <div class="col-sm-12" style="text-align:center;">
                                    <button class="btn btn-default" type="submit" style="width:100%;">
                                        <span class="glyphicon glyphicon-save"></span> Simpan
                                    </button>
                            </div>
                         </div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<div class="row example-screen">
	<div class="col-sm-1">
        <a href="#" onclick="window.print();"><button class="btn btn-sm btn-default panel-title" style="margin:5px; width:100%;"><span class="glyphicon glyphicon-print"></span> Cetak </button></a>
	</div>
    <div class="col-sm-2">
        <a href="#openModalindikatorJudul"><button class="btn btn-sm btn-default panel-title" style="width:100%; margin:5px;"><span class="glyphicon glyphicon-plus"></span> Judul Kegiatan </button></a>
	</div>
    <div class="col-sm-2">
        <a href="#openModalindikator"><button class="btn btn-sm btn-default panel-title" style="width:100%; margin:5px;"><span class="glyphicon glyphicon-plus"></span> Data Kegiatan </button></a>
	</div>
    <div class="col-sm-2" style="float:right;">
            <select class="form-control" style="margin:5px;" tabindex="3" onchange="window.open(this.options[this.selectedIndex].value,'_top')">
                <option value="">Periode : <?php echo $blnnyayeuh; ?> </option>
                <option value=""> ------------------------------ </option>
                <?php 
				for($x=1; $x<=12; $x++){
				$bulan = getbulan($x); ?>
					<option value="?page=renjabulanantu&bulan=<?php echo $x; ?>&tahun=<?php echo $tahun_kegiatan; ?>"> 
						<?php echo "$bulan"; ?>
                    </option>
				<?php } ?>	
            </select>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">

    	<div class="table-responsive-vertical shadow-z-1" style="padding-top:5px;">
		<?php
		$daftar_pusat_tampil = mysql_query("SELECT * FROM pusat WHERE id_pusat=$id_pusat_kegiatan");
        $tp=mysql_fetch_array($daftar_pusat_tampil);
        ?>
        
        <h4 style="text-align:center; text-transform:uppercase; line-height: 150%;">
            LAPORAN REALISASI KINERJA DAN ANGGARAN <br />
			UNIT / ESELON II <?php echo $tp['nama_pusat']; ?><br />
            PERIODE : <?php echo $blnnyayeuh; ?>
            <br /> TAHUN ANGGARAN <?php echo $tahun_kegiatan; ?>
			
        </h4>
        
        	<?php $persen_bulan = mysql_query("SELECT * FROM realisasi_kegiatan_bulanan a, realisasi_kegiatan b, realisasi_sasaran c 
					WHERE a.id_kegiatan=b.id_kegiatan AND b.id_realisasi=c.id_realisasi AND c.id_pusat=$tp[id_pusat] AND a.tahun=$tahun_kegiatan AND a.bulan=$bln_kegiatan"); 
				  $jml_keg = mysql_num_rows($persen_bulan);
				  $total_persen_kegiatan_bulan=0;
				  while($rk=mysql_fetch_array($persen_bulan)){
					  $total_persen_kegiatan_bulan=$total_persen_kegiatan_bulan+$rk['kinerja_realisasi']; 
				  }
				  $total_bulan_ini=$total_persen_kegiatan_bulan/$jml_keg;
			?>
            <span style="float:right; margin:0 20px 5px 0;">Realisasi Kinerja /<?php echo $blnnyayeuh; ?> : <b><?php echo $total_bulan_ini; ?> %</b></span>	
          <!-- Table starts here -->
          <table id="table" class="table table-hover table-bordered" style="margin-bottom:100px;">
              <thead style="text-transform:uppercase;">
                  <tr>
                    <th>No</th>
                    <th width="1%">Indikator Kinerja</th>
                    <th colspan="2">Kegiatan</th>
                    <th width="5%">Target Kinerja (%)</th>
                    <th width="5%">Realisasi Kinerja (%)</th>
                    <th width="10%">Realisasi Anggaran</th>
                    <th>Realisasi Target Kinerja & Output Bulan Berjalan</th>
                    <th width="5%">Hambatan</th>
                    <th width="5%">Solusi</th>
                    <th width="5%">Data Dukung</th>
                    <th width="3%" class="example-screen"></th>
                  </tr>
                  <tr>
                    <th>1</th>
                    <th>2</th>
                    <th colspan="2">3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th class="example-screen"></th>
                  </tr>
              </thead>
              <tbody style="font-size:12px;">
              	<?php
				$tampil = mysql_query("SELECT * FROM realisasi_sasaran WHERE tahun=$tahun_kegiatan AND id_pusat=$tp[id_pusat] AND id_realisasi_parent != 0 ORDER BY id_realisasi ASC");
				$no=1;
				while($s=mysql_fetch_array($tampil)){
				$tampil_kegiatan=mysql_query("SELECT * FROM realisasi_kegiatan 
											 WHERE id_realisasi=$s[id_realisasi] AND id_kegiatan_parent = 0 ORDER BY id_kegiatan ASC");
				$cek_jml_kegiatan=mysql_num_rows($tampil_kegiatan);
				?>
                    <tr style="font-weight:bolder; text-transform:uppercase;">
                      	<td data-title="No" width="1%" align="center"><?php echo $no; ?></td>
                        <td data-title="Sasaran Strategis" colspan="10">
							<?php echo $s['judul_realisasi']; ?>
                        </td>
                    </tr>
                <?php 	$no_keg=1;
						while($r=mysql_fetch_array($tampil_kegiatan)){ ?>
                			<tr>
                                <td style=" border:none;"></td>
                                <td style=" border:none;"></td>
                                <td width="1%"><b>05<?php echo $no_keg; ?>-</b></td>
                                <td data-title="Kegiatan">
                                <b>						<?php echo $r['judul_kegiatan']; ?>		</b>
                                <?php echo "<a href='?page=renjabulanantu&act=edit&id=$r[id_kegiatan]' title='Edit $r[judul_kegiatan]' class='example-screen'>
                                                        <span class='glyphicon glyphicon-refresh' style='margin-left:10px;'></span></a>"; ?>
                                                        
                                <?php echo "<a href=\"?page=renjabulanantu&act=hapus&id=$r[id_kegiatan]\"onClick=\"return confirm ('Anda Yakin akan menghapus Kegiatan $r[judul_kegiatan]')\"  class='example-screen'>
                                                        <span class='glyphicon glyphicon-trash' style='margin-left:10px;' title='Delete Kegiatan $r[judul_kegiatan]'></span></a>"; ?>                       
                                </td>
                                <?php
								$tampil_kd1=mysql_query("SELECT * FROM realisasi_kegiatan_bulanan WHERE id_kegiatan=$r[id_kegiatan] AND tahun=$tahun_kegiatan AND bulan=$bln_kegiatan");
								$tkd1=mysql_fetch_array($tampil_kd1);
								$cek_tkd1=mysql_num_rows($tampil_kd1);
								?>
                                <td data-title="Target (%)" align="center">	<b>	<?php if($tkd1['kinerja_target']!=0){ echo $tkd1['kinerja_target']." %"; } ?>		</b></td>
                                <td data-title="Realisasi (%)" align="center"><b>	<?php if($tkd1['kinerja_realisasi']!=0){ echo $tkd1['kinerja_realisasi']." %"; } ?>	</b></td>
                                <td data-title="Realisasi Anggaran" align="center">
                                    <b>	<?php if($tkd1['kinerja_anggaran']!=0){ ?>
                                                Rp. <span style="float:right;"><?php echo rupiah($tkd1['kinerja_anggaran']);?>,-</span>
                                        <?php } ?>
                                    </b>
                                </td>
                                <td data-title="Output Bulanan">				<?php echo $tkd1['output_blnan']; ?>		</td>
                                <td data-title="Hambatan">						<?php echo $tkd1['hambatan']; ?>			</td>
                                <td data-title="Solusi">						<?php echo $tkd1['solusi']; ?>				</td>
                                <td data-title="Data Dukung">					<?php echo $tkd1['data_dukung']; ?>		</td>
                                <td data-title="Aksi Data">
                                <?php if($cek_tkd1!=0){ ?>
                                <?php //echo "<a href='?page=renjabulanantu&act=edit_bulanan&id=$tkd1[id_kegiatan_bulanan]' title='Edit Data Bulanan $r[judul_kegiatan]'>
                                      //                  <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit Edit Data Bulanan Kegiatan $r[judul_kegiatan]'></a>"; ?>
                                                        
                                <?php echo "<a href=\"?page=renjabulanantu&act=hapus_bulanan&id=$tkd1[id_kegiatan_bulanan]\"onClick=\"return confirm ('Anda Yakin akan menghapus Data Bulanan Kegiatan $r[judul_kegiatan]')\"  class='example-screen'>
                                                        <span class='glyphicon glyphicon-trash' style='margin-left:10px;' title='Delete Data Bulanan Kegiatan $r[judul_kegiatan]'></a>"; ?>                       
                                <?php } ?>
                                </td>
                            </tr>
                            <?php
							$cek_kegiatan_parent = mysql_query("SELECT * FROM realisasi_kegiatan WHERE id_kegiatan_parent=$r[id_kegiatan] ORDER BY id_kegiatan ASC");
							$cekjml=mysql_num_rows($cek_kegiatan_parent);
							if($cekjml!=0){?>	  
							<?php $no_keg1='A';
								  while($lparent=mysql_fetch_array($cek_kegiatan_parent)){?>
                                		<tr>
                                            <td style=" border:none;"></td>
                                			<td style=" border:none;"></td>
                                            <td width="1%"><?php echo $no_keg1; ?>.</td>
                                            <td data-title="Kegiatan">
                                            	<?php echo $lparent['judul_kegiatan']; ?>	<br />	
                                            	<?php echo "<a href='?page=renjabulanantu&act=edit&id=$lparent[id_kegiatan]' title='Edit $lparent[judul_kegiatan]'  class='example-screen'>
																		<span class='glyphicon glyphicon-refresh' style='margin-left:10px;'></span></a>"; ?>
																		
												<?php echo "<a href=\"?page=renjabulanantu&act=hapus&id=$lparent[id_kegiatan]\"onClick=\"return confirm ('Anda Yakin akan menghapus Kegiatan $lparent[judul_kegiatan]')\"  class='example-screen'>
																	<span class='glyphicon glyphicon-trash' style='margin-left:10px;' title='Delete Kegiatan $lparent[judul_kegiatan]'></span></a>"; ?>   
                                            </td>
                                            <?php
											$tampil_kd2=mysql_query("SELECT * FROM realisasi_kegiatan_bulanan WHERE id_kegiatan=$lparent[id_kegiatan] AND tahun=$tahun_kegiatan AND bulan=$bln_kegiatan");
											$tkd2=mysql_fetch_array($tampil_kd2);
											$cek_tkd2=mysql_num_rows($tampil_kd2);
											?>
                                            <td data-title="Target (%)" align="center"> <b>		<?php if($tkd2['kinerja_target']!=0){ echo $tkd2['kinerja_target']." %"; } ?>		</b></td>
                                            <td data-title="Realisasi (%)" align="center"><b>	<?php if($tkd2['kinerja_realisasi']!=0){ echo $tkd2['kinerja_realisasi']." %"; } ?>	</b></td>
                                            <td data-title="Realisasi Anggaran" align="center">
                                                <b>	<?php if($tkd2['kinerja_anggaran']!=0){ ?>
                                                            Rp. <span style="float:right;"><?php echo rupiah($tkd2['kinerja_anggaran']);?>,-</span>
                                                    <?php } ?>
                                                </b>
                                            </td>
                                            <td data-title="Output Bulanan">				<?php echo $tkd2['output_blnan']; ?>		</td>
                                            <td data-title="Hambatan">						<?php echo $tkd2['hambatan']; ?>			</td>
                                            <td data-title="Solusi">						<?php echo $tkd2['solusi']; ?>				</td>
                                            <td data-title="Data Dukung">					<?php echo $tkd2['data_dukung']; ?>		</td>
                                            <td data-title="Aksi Data">
                                            <?php if($cek_tkd2!=0){ ?>
											<?php // echo "<a href='?page=renjabulanantu&act=edit_bulanan&id=$tkd2[id_kegiatan_bulanan]' title='Edit Data Bulanan $lparent[judul_kegiatan]'>
                                                  //                  <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit Edit Data Bulanan Kegiatan $lparent[judul_kegiatan]'></a>"; ?>
                                            <?php echo "<a href=\"?page=renjabulanantu&act=hapus_bulanan&id=$tkd2[id_kegiatan_bulanan]\"onClick=\"return confirm ('Anda Yakin akan menghapus Data Bulanan Kegiatan $lparent[judul_kegiatan]')\"  class='example-screen'>
                                                                    <span class='glyphicon glyphicon-trash' style='margin-left:10px;' title='Delete Data Bulanan Kegiatan $lparent[judul_kegiatan]'></span></a>"; ?>                       
                                            <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
										$cek_kegiatan_parent1 = mysql_query("SELECT * FROM realisasi_kegiatan WHERE id_kegiatan_parent=$lparent[id_kegiatan] ORDER BY id_kegiatan ASC");
										$cekjml1=mysql_num_rows($cek_kegiatan_parent1);
										if($cekjml1!=0){?>	  
										<?php $no_keg2='a';
											  while($lparent1=mysql_fetch_array($cek_kegiatan_parent1)){?>
													<tr>
														<td style=" border:none;"></td>
														<td style=" border:none;"></td>
														<td width="1%"><?php echo $no_keg2; ?>.</td>
														<td data-title="Kegiatan">
                                                        	<?php echo $lparent1['judul_kegiatan']; ?>
                                                          	<?php echo "<a href='?page=renjabulanantu&act=edit&id=$lparent1[id_kegiatan]' title='Edit $lparent1[judul_kegiatan]'  class='example-screen'>
																					<span class='glyphicon glyphicon-refresh' style='margin-left:10px;'></span></a>"; ?>
																					
															<?php echo "<a href=\"?page=renjabulanantu&act=hapus&id=$lparent1[id_kegiatan]\"onClick=\"return confirm ('Anda Yakin akan menghapus Kegiatan $lparent1[judul_kegiatan]')\"  class='example-screen'>
																					<span class='glyphicon glyphicon-trash' style='margin-left:10px;' title='Delete Kegiatan $lparent1[judul_kegiatan]'></span></a>"; ?> 
                                                        </td>
														<?php
														$tampil_kd3=mysql_query("SELECT * FROM realisasi_kegiatan_bulanan WHERE id_kegiatan=$lparent1[id_kegiatan] AND tahun=$tahun_kegiatan AND bulan=$bln_kegiatan");
														$tkd3=mysql_fetch_array($tampil_kd3);
														$cek_tkd3=mysql_num_rows($tampil_kd3);
														?>
                                                        <td data-title="Target (%)" align="center"> <b>	<?php if($tkd3['kinerja_target']!=0){ echo $tkd3['kinerja_target']." %"; } ?></b></td>
														<td data-title="Realisasi (%)" align="center"><b><?php if($tkd3['kinerja_realisasi']!=0){ echo $tkd3['kinerja_realisasi']." %"; } ?></b></td>
														<td data-title="Realisasi Anggaran" align="center">
                                                            <b>	<?php if($tkd3['kinerja_anggaran']!=0){ ?>
                                                                        Rp. <span style="float:right;"><?php echo rupiah($tkd3['kinerja_anggaran']);?>,-</span>
                                                                <?php } ?>
                                                            </b>
                                                        </td>
                                                        <td data-title="Output Bulanan">				<?php echo $tkd3['output_blnan']; ?>		</td>
														<td data-title="Hambatan">						<?php echo $tkd3['hambatan']; ?>			</td>
														<td data-title="Solusi">						<?php echo $tkd3['solusi']; ?>				</td>
														<td data-title="Data Dukung">					<?php echo $tkd3['data_dukung']; ?>		</td>
                                                        <td data-title="Aksi Data">
                                                        <?php if($cek_tkd3!=0){ ?>
														<?php //echo "<a href='?page=renjabulanantu&act=edit_bulanan&id=$tkd3[id_kegiatan_bulanan]' title='Edit Data Bulanan $lparent1[judul_kegiatan]'>
                                                                     //           <img src=../images/update.png alt=edit border=0 class='icon_hareup' title='Edit Edit Data Bulanan Kegiatan $lparent1[judul_kegiatan]'></a>"; ?>
                                                        <?php echo "<a href=\"?page=renjabulanantu&act=hapus_bulanan&id=$tkd3[id_kegiatan_bulanan]\"onClick=\"return confirm ('Anda Yakin akan menghapus Data Bulanan Kegiatan $lparent1[judul_kegiatan]')\"  class='example-screen'>
                                                                                <span class='glyphicon glyphicon-trash' style='margin-left:10px;' title='Delete Data Bulanan Kegiatan $lparent1[judul_kegiatan]'></span></a>"; ?>                       
                                                        <?php } ?>
                                                        </td>
													</tr>
										<?php $no_keg2++; 
											  }//end while daftar kegiatan ketiga ?>
										<?php 
										}//end if cek daftar turunan kedua ?>
                            <?php 	$no_keg1++;
									}//end while daftar kegiatan kedua ?>
                            <?php 
							}//end if cek daftar turunan pertama ?>
                <?php 	$no_keg++;
						} //tutup while tampil Kegiatan pertama?>
                <?php $no++;
				} // tutup while daftar indikator kinerja?>
              </tbody>
            </table>
            <!--<div class="example-print" style="margin-bottom:1000px;"></div>-->     
    	</div>
    </div>
</div>
<?php
break;
//==========================================================================================================================
case "tambah";
if($_POST[tipe]=='bab'){
	$judul_kegiatan= strtoupper($_POST['judul_kegiatan']);
	mysql_query("INSERT INTO realisasi_kegiatan(id_realisasi, id_kegiatan_parent, judul_kegiatan, type, id_tu_upload, tgl_upload) 
						VALUES('$_POST[id_realisasi]', '$_POST[id_kegiatan_parent]', '$judul_kegiatan', '$_POST[type]', '$_SESSION[id_user]', '$tgl_sekarang')");
						
}else{
	$cek_data = mysql_num_rows(mysql_query("SELECT * FROM realisasi_kegiatan_bulanan WHERE id_kegiatan=$_POST[id_kegiatan] AND tahun = $_POST[tahun] AND bulan = $_POST[bulan]"));
	$cek_bab = mysql_fetch_array(mysql_query("SELECT * FROM realisasi_kegiatan WHERE id_kegiatan=$_POST[id_kegiatan]"));
	if($cek_data==0 && $cek_bab[type]!='bab'){
		$lokasi_file    = $_FILES['fupload']['tmp_name'];
		$tipe_file      = $_FILES['fupload']['type'];
		$nama_file      = $_FILES['fupload']['name'];
		move_uploaded_file($lokasi_file,"data_dukung/$nama_file");
		mysql_query("INSERT INTO realisasi_kegiatan_bulanan(id_kegiatan, tahun, bulan, id_tu_upload, tgl_upload,
													kinerja_target, kinerja_realisasi, kinerja_anggaran, output_blnan, hambatan, solusi, data_dukung) 
							VALUES('$_POST[id_kegiatan]', '$_POST[tahun]', '$_POST[bulan]', '$_SESSION[id_user]', '$tgl_sekarang',
									'$_POST[kinerja_target]', '$_POST[kinerja_realisasi]',  '$_POST[kinerja_anggaran]','$_POST[output_blnan]', '$_POST[hambatan]','$_POST[solusi]', '$nama_file')");
	}
}

echo "<script language='JavaScript'>location.href='?page=renjabulanantu&bulan=$_POST[bulan]';</script>";
//echo"'$_POST[id_realisasi_parent]', '$_POST[id_pusat]', '$_POST[judul_realisasi]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";echo ucwords("hello world");

break;
//=============================================================================================================================
case"edit";
$edit = mysql_query("SELECT a.*, b.id_realisasi FROM realisasi_kegiatan a, realisasi_sasaran b WHERE a.id_realisasi=b.id_realisasi AND a.id_kegiatan = $_GET[id]");
$r= mysql_fetch_array($edit);
?>
<form method='POST' action='?page=renjabulanantu&act=editnya'  enctype='multipart/form-data'>
	<input type='hidden' name='id' value='<?php echo $r['id_kegiatan']; ?>'>
    <div class="panel panel-default">
        <div class="panel-heading" 
            style="background: -webkit-linear-gradient(to bottom right, #efefef 50%, #fff 50%);	background: linear-gradient(to bottom right, #efefef 50%, #fff 50%); background-size:cover;
            background-attachment: fixed;">
            <h4 class="panel-title" style="text-align:center; text-transform:uppercase;">Perubahan Data Kegiatan</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-sm-12">
                        	Pilih Indikator Kinerja
                            <select class="form-control realisasi" name="id_realisasi" >
                                <?php
                                $daftar_indikator = mysql_query("SELECT * FROM realisasi_sasaran WHERE tahun=$_SESSION[tahun] AND id_pusat=$_SESSION[id_pusat] AND id_realisasi_parent != 0 
                                                                 ORDER BY id_realisasi ASC");
                                while ($i=mysql_fetch_array($daftar_indikator)){ 
                                	if ($r[id_realisasi]==$i[id_realisasi]){ ?>
										<option value="<?php echo $i['id_realisasi'];?>" selected="selected"> <?php echo $i['judul_realisasi']; ?> </option>
							  <?php }
									else{ ?>
										<option value="<?php echo $i['id_realisasi'];?>"> <?php echo $i['judul_realisasi']; ?> </option>
							  <?php	}
								
								} ?>
                            </select> 
                        </div>
                        <div class="col-sm-12">
                        	<?php $elp = mysql_fetch_array(mysql_query("SELECT * FROM realisasi_kegiatan WHERE id_kegiatan=$r[id_kegiatan_parent]")); ?>
                            Pilih Jenis Kepala Kegiatan
                            <select class="form-control kegiatan_parent" name="id_kegiatan_parent">
                       	<?php if($r['id_kegiatan_parent'] != 0) {  ?>
								<option selected="selected" value="<?php echo $r['id_kegiatan_parent'];?>"><?php echo $elp['judul_kegiatan'];?></option>
						<?php } else { ?>
                        		<option selected="selected"> -- Pilih Kepala Kegiatan -- </option>
                        <?php }?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            Tipe Kegiatan
                            <select class="form-control" name="type">
                                <option value="">Data Kegiatan</option>
                                <option value="bab" <?php if($r['type']=='bab'){?> selected="selected" <?php } ?>>Kepala Kegiatan</option>
                            </select> 
                        </div>
                        <div class="col-sm-9">
                            Judul Kegiatan
                            <input name='judul_kegiatan' class="form-control" type='text' placeholder='Judul Kegiatan' required="" value="<?php echo $r['judul_kegiatan']; ?>"/>
                        </div> 
                    </div>
                </div><!-- tutup col12-->
         </div>
         <div class="row">
            <div class="col-sm-6" style="text-align:center;">
                    <button class="btn btn-default" type="submit" style="width:100%;">
                        <span class="glyphicon glyphicon-save"></span> Simpan
                    </button>
            </div>
            <div class="col-sm-6" style="text-align:center;">
                    <a href="?page=renjabulanantu">
                    <span class="btn btn-default" style="width:100%;">
                        <span class="glyphicon glyphicon-step-backward"></span> Kembali
                    </span>
                    </a>
            </div>
         </div>
        </div>
    </div>
</form>
<?php
break;
//=====================================================================================================================================
case "editnya";
if($_POST[type]=='bab'){ $judul_kegiatan= strtoupper($_POST['judul_kegiatan']); }else{ $judul_kegiatan= $_POST['judul_kegiatan'];}

	mysql_query("UPDATE realisasi_kegiatan SET 	id_kegiatan_parent  = '$_POST[id_kegiatan_parent]', id_realisasi  = '$_POST[id_realisasi]',
												judul_kegiatan  = '$judul_kegiatan', type = '$_POST[type]'
				  WHERE id_kegiatan     = '$_POST[id]'");
				  
	echo "<script language='JavaScript'>location.href='?page=renjabulanantu&act=edit&id=$_POST[id]';</script>";


//echo"'$_POST[id_realisasi_parent]', '$_POST[id_pusat]', '$_POST[judul_realisasi]', '$_POST[tahun]', '$_POST[type]', '$tgl_sekarang'";

break;
//================================================================================================================================
case "hapus";
mysql_query("DELETE FROM realisasi_kegiatan WHERE id_kegiatan='$_GET[id]'");

echo "<script language='JavaScript'>location.href='?page=renjabulanantu';</script>";

break;
//================================================================================================================================
case "hapus_bulanan";
mysql_query("DELETE FROM realisasi_kegiatan_bulanan WHERE id_kegiatan_bulanan='$_GET[id]'");

echo "<script language='JavaScript'>location.href='?page=renjabulanantu';</script>";

break;
//===================================================================================================================================
}
?>
