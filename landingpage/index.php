<?php
include "../config/koneksi.php";
include "../config/library.php";
include "../config/fungsi_indotgl.php";
include "../config/fungsi_combobox1.php";
include "../config/class_paging.php";
include "../config/rupiah.php";

session_start();
if (empty($_SESSION[id_user])){
	echo "<script language='JavaScript'> location.href='../login.php?login=login'; </script>";
}
else{
	if($_SESSION['type_user']=='admin'){
		$query=mysql_query("SELECT * FROM datin_laporan WHERE type != 'bab' AND pending=1 AND file_fulltext is NULL");
	}else{
		$query=mysql_query("SELECT * FROM datin_laporan WHERE id_pusat=$_SESSION[id_pusat] AND approve=0 AND type != 'bab'");
	}
	$jl= mysql_num_rows($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Aplikasi PPL - BPHN</title>
<meta charset="utf-8">
      <link rel="stylesheet" href="../config/css/components.css">
      <link rel="stylesheet" href="../config/css/responsee.css">
      
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	  <script type="text/javascript" src="../config/js/jquery-1.8.3.min.js"></script>
      <script type="text/javascript" src="../config/js/jquery-ui.min.js"></script>    
      <script type="text/javascript" src="../config/js/responsee.js"></script>
      <link rel="shortcut icon" href="http://www.bphn.go.id/public/bphn/images/icon.png">
</head>

<body class="size-1140">
      <header>
         <div class="line" style="margin-bottom:20px; padding-top:20px;">
               <div class="s-12 l-12" style="margin-top:5px; text-align:center;">
                   <a href="http://www.bphn.go.id">
                        <img src="header.png"
                          	title="&quot;AYO KERJA, KAMI PASTI&quot; ( Profesional Akuntabel Sinergi Transparan Inovatif ) &loz; Badan Pembinaan Hukum Nasional - Kemenkumham RI">
                   </a>
               </div>
        </div>
      </header>     
      <section>     
      <!-- HOME PAGE BLOCK -->      
         <div class="line">
            <div class="margin">
               <div class="s-12 l-4 margin-bottom">
               <a href="../e-disposisi/">
                      <div class="box box_hareup" style="text-align:center;">
                         <div class="titlebox">E-Disposisi <?php if($jl!=0){echo "<notif>".$jl."</notif>"; }?></div>
                         <img src="e-disposisi.png" class="icon_hareup">                  
                      </div>
               	  </a>
               </div>
               
               <div class="s-12 l-4 margin-bottom">
                  <a href="../datin/">
                      <div class="box box_hareup" style="text-align:center;">
                         <div class="titlebox">Laporan Datin <?php if($jl!=0){echo "<notif>".$jl."</notif>"; }?></div>
                         <img src="datin.png" class="icon_hareup">                  
                      </div>
               	  </a>
               </div>
               
               <div class="s-12 l-4 margin-bottom">
                  <a href="../realisasi/">
                      <div class="box box_hareup" style="text-align:center;">
                         <div class="titlebox">Laporan Realisasi</div>
                         <img src="lakip.png" class="icon_hareup">                  
                      </div>
               	  </a>
               </div>
               
               <div class="s-12 l-3 margin-bottom">
               </div>
               
               <!--<div class="s-12 l-3 margin-bottom">
                  <a href="http://www.bphn.go.id">
                      <div class="box box_hareup" style="text-align:center;">
                         <div class="titlebox">E-Disposisi</div>
                         <img src="e-disposisi.png" class="icon_hareup">                  
                      </div>
               	  </a>
               </div> 
               
               <div class="s-12 l-3 margin-bottom">
                  <a href="http://www.bphn.go.id">
                      <div class="box box_hareup" style="text-align:center;">
                         <div class="titlebox">R. Rapat</div>
                         <img src="rapat.png" class="icon_hareup">                  
                      </div>
               	  </a>
               </div>-->              
            
           </div>
         </div>
      </section>
      <!-- FOOTER -->
      <footer class="line">
            <div class="s-12 l-12" style="text-align:center; margin-top:20px; font-size:12px; font-style:italic;">
               <p style="color:#FFFFFF;">
               		 <span style="text-transform:uppercase; font-weight:bold;">&nbsp;&nbsp; Bagian Program dan Pelaporan</span>
                     <br>Copyright &copy; 2017 - All Rights Reserved 
               </p>
            </div>
	  </footer>
</body>
</html>
<?php } ?>