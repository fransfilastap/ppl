<!DOCTYPE html>
<html>
<head>
<title>Program dan Pelaporan BPHN</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="http://www.bphn.go.id/public/bphn/images/icon.png">
<link href="config/login_style.css" rel="stylesheet" type="text/css" media="all" />
<link href='/fonts.googleapis.com/css?family=Text+Me+One' rel='stylesheet' type='text/css'>

</head>
<body>
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<div class="main-agileinfo">
			<div class="agileits-top"> 
				<form action="cek_login.php" method="post"> 
					<input class="text" type="text" name="username" id="username" placeholder="Username" required="">
					<input class="text" type="password" name="password" id="password" placeholder="Password" required="">
					
					<input type="submit" value="LOGIN">
				</form>
                <script type="text/javascript">
					var blink_speed = 500; 
					var t = setInterval(function () { 
						var ele = document.getElementById('blinker'); ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden'); 
					}, blink_speed);
				</script> 
				<?php if($_GET[login]=='failed'){  
				echo "<center>
						<span class='title' id='blinker' style='color:#FF0000; font-size:12px; text-transform:uppercase; font-weight:bold;
							background-color:#FFFFFF; padding:10px; border-radius: 10px;'>
							<i>Username dan Password tidak ditemukan!</i>
						</span>
					  </center>";
				} ?>
				<?php if($_GET[login]=='login'){  
				echo "<center>
						<span class='title' id='blinker' style='color:#FF0000; font-size:12px; text-transform:uppercase; font-weight:bold;
							background-color:#FFFFFF; padding:10px; border-radius: 10px;'>
							<i>Anda Harus Login terlebih dahulu!</i>
						</span>
					  </center>";
				} ?>                
			</div>	 
		</div>	

		<ul class="w3lsg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>	
	<!-- //main --> 
</body>
</html>